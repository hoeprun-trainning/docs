# Linux基础知识培训

## 目录

[资料推荐](#资料推荐)

[Linux环境配置](#Linux环境配置)

[Linux的常用命令介绍](#Linux的常用命令介绍)

[vim基础介绍](#vim基础介绍)

[shell编程一](#shell编程一)

[shell编程二](#shell编程二)

[Makefile基础知识讲解](#Makefile基础知识讲解)

[Git实操介绍](#Git实操介绍)

## 资料推荐<a name="资料推荐"></a>

### 书籍推荐

推荐书籍：

shell：《跟老男孩学Linux运维》

C++书籍推荐：《C++新经典》王健伟
鸿蒙推荐书籍：《OpenHarmony——编译、内核、驱动及应用开发全栈》李传钊

书籍仅作参考使用，不做任何宣传

### 视频推荐

[MOOC大学Linux系统管理课程](https://www.icourse163.org/course/NBCC-437004?tid=1002357007)
[玩转Vim 从放弃到爱不释手](https://www.imooc.com/learn/1129)

## 一、Linux环境配置<a name="Linux环境配置"></a>

### 1.1 Unix和Linux的简介

**Unix发展历史**

- 1965年，美国麻省理工学院（ MIT）、通用电气公司（ GE）及AT&T的贝尔实验室联合开发Multics工程计划，其目标是开发一种交互式的具有多道程序处理能力的分时操作系统，但因Multics追求的目标过于庞大复杂，项目进度远远落后于计划，最后贝尔实验室宣布退出。
- 1969年，美国贝尔实验室的肯 • 汤普森在DECPDP-7机器上开发出了UNIX系统。
- 1971年，肯 • 汤普森的同事丹尼斯•里奇发明了C语言； 1973年， UNIX系统的绝大部分源代码用C语言重写，这为提高UNIX系统的可移植性打下基础。

**Unix的主要发行版本**

| 操作系统    | 公司                               | 硬件平台                             |
| ----------- | ---------------------------------- | ------------------------------------ |
| AIX         | IBM                                | PowerPC                              |
| UX          | HP                                 | PA-RISC                              |
| Solaris     | SUN                                | SPARC                                |
| Linux、 BSD | Red Hat Linux 、 Ubuntu 、 FreeBSD | IA（ Intel、 AMD、 Cyrix、 RISE...） |

**Linux发展历史**

- Linux系统诞生于1991年，由芬兰大学生林纳斯（Linus Torvalds）和后来陆续加入的众多爱好者共同开发完成。
- Linux是自由软件，源代码开放的UNIX

**Linux版本分类**

![image-20221215114212474](../img/linux_base/README/1.png)

![image-20221215114246514](../img/linux_base/README/2.png)

**Linux的应用领域**

- 基于Linux的服务器
- Linux在电影娱乐业
- Linux在嵌入式领域

### 1.2 虚拟机和WSL的安装

#### 1.2.1 虚拟机的安装

除非电脑配置很高，否则当前不太推荐，这里不介绍，仅提供工具下载链接，想了解的可以网上查看.

[Ubuntu镜像下载](http://mirrors.163.com/ubuntu-releases/)
[VMware下载](http://mirrors.163.com/ubuntu-releases/)

#### 1.2.2 WSL的安装

[Win10电脑安装配置WSL指导](../env/wsl_env_config.md)

### 1.3 软件安装和卸载

**软件安装**

- 源码安装
- apt-get install安装
  - apt-get -y: 安装并确认
  - apt-get -f: 强制安装

**软件卸载**

- apt-get autoremove

**基本环境配置**

[Ubuntu环境搭建](../env/ubuntu_env_config.md)

## 二、Linux的常用命令介绍<a name="Linux的常用命令介绍"></a>

### 2.1 Linux文件类型

| 文件类型     | flag | 创建方式                               |
| ------------ | ---- | -------------------------------------- |
| 普通文件     | -    | touch a.txt                            |
| 目录文件     | d    | mkdir a                                |
| 链接文件     | l    | ln -s ./b.txt a.txt & ln ./b.txt a.txt |
| 块设备文件   | b    | -                                      |
| 字符设备文件 | c    | -                                      |
| 管道文件     | p    | mkfifo fifo_file                       |
| 套接字文件   | s    | -                                      |

### 2.2 Linux目录层级介绍

| 目录             | 目录描述                                                     |
| ---------------- | ------------------------------------------------------------ |
| /bin             | 存放系统命令的目录，普通用户和超级用户都可以执行。是/usr/bin目录的软 链接 |
| /sbin            | 存放系统命令的目录，只有超级用户才可以执行。是/usr/sbin目录的软链接 |
| /usr/bin         | 存放系统命令的目录，普通用户和超级用户都可以执行             |
| /usr/sbin        | 存放系统命令的目录，只有超级用户才可以执行                   |
| /boot            | 系统启动目录，保存与系统启动相关的文件，如内核文件和启动引导程序 （grub）文件等 |
| /dev             | 设备文件保存位置                                             |
| /etc             | 配置文件保存位置。系统内所有采用默认安装方式（apt安装）的服务配置文 件全部保存在此目录中，如用户信息、服务的启动脚本、常用服务的配置文件 等 |
| /home            | 普通用户的家目录。创建用户时，每个用户要有一个默认登录和保存自己数据 的位置，就是用户的家目录 |
| /lib             | 系统调用的函数库保存位置，是/usr/lib的软链接                 |
| /lib64           | 64位函数库保存的位置。是/usr/lib64的软链接                   |
| /lost+found      | 当系统意外崩溃或机器意外关机而产生的一些文件碎片放在这里     |
| /media           | 挂载目录。系统建议是用来挂载媒体设备的，如软盘和光盘         |
| /misc            | 挂载目录。系统建议用来挂载NFS服务的共享目录                  |
| /mnt             | 挂载目录。系统建议用来挂载额外的设备                         |
| /opt             | 第三方安装的软件保存位置                                     |
| /proc            | 虚拟文件系统。该目录中的数据并不保存在硬盘上，而是保存在内存中。主要 保存系统的内核、进程、外部设备状态和网络状态等 |
| /sys             | 虚拟文件系统。和/proc目录相似，该目录中的数据都保存在内存中，主要保 存与内核相关的信息 |
| /root            | root的宿主目录。                                             |
| /run             | 系统运行时产生的数据，如ssid，pid等相关数据。/var/run是此目录的软链 接 |
| /srv             | 服务数据目录，一些系统服务启动后可以在这个目录中保存所需要的数据 |
| /tmp             | 临时文件目录。所有用户都能访问和写入。不能保存重要数据       |
| /usr             | 系统软件资源目录。"Unix Software Resource"缩写。系统中安装的软件大部 分保存在这里 |
| /usr/lib         | 应用程序调用的函数库保存位置                                 |
| /usr/local       | 手工安装的软件保存位置。我们一般建议源码包软件安装在这个位置 |
| /usr/share       | 应用程序的资源文件保存位置，如帮助文档、说明文档和字体目录   |
| /usr/src         | 源码包保存位置                                               |
| /usr/src/kernels | 内核源码保存位置                                             |
| /var             | 动态数据保存位置。主要保存缓存、日志以及软件运行所产生的文件 |
| /var/www/html    | apt安装的apache的网页主目录                                  |
| /var/lib         | 程序运行中需要调用或改变的数据保存位置                       |
| /var/log         | 系统日志保存位置                                             |
| /var/run         | 一些服务和程序运行后，它们的PID保存位置，/run目录的软链接位置 |
| /var/spool       | 放置队列数据的目录                                           |
| /var/spool/mail  | 新收到的邮件队列保存位置。系统新收到的邮件会保存在此目录中   |
| /var/spool/cron  | 系统的定时任务队列保存位置。系统的计划任务会保存在这里       |

### 2.3 man（manual）功能使用

man [man options] [[section] page ...] ...

**常用方法**

man 2 read
不同章节的结果可能存在不同的区别
man 1 read
man 2 read
**man的章节含义**

| section | description                                                  | 备注                                      |
| ------- | ------------------------------------------------------------ | ----------------------------------------- |
| 1       | Executable programs or shell commands                        | 可运行的指令或可执行文件 的帮助文档       |
| 2       | System calls (functions provided by the kernel)              | 系统核心可使用的函数与工 具等的帮助文档   |
| 3       | Library calls (functions within program libraries)           | 一些常用的函数与库的帮助 文档             |
| 4       | Special files (usually found in /dev)                        | 设备文件的说明                            |
| 5       | File formats and conventions, e.g. /etc/passwd               | 配置文件或者是某些文件的 格式说明         |
| 6       | Games                                                        | 游戏程序的帮助文档                        |
| 7       | Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7) | 惯例与协议，如文件系统、 网络协议等的说明 |
| 8       | System administration commands (usually only for root)       | 系统管理员可用的管理命令 的帮助文档       |
| 9       | Kernel routines [Non standard]                               | 跟系统核心有关的文件的帮 助文档           |

**man命令界面的字段说明**

| 字段          | 描述                                     |
| ------------- | ---------------------------------------- |
| NAME          | 这个命令(程序)的名称和简单用途的说明     |
| SYNOPSIS      | 命令(程序)语法                           |
| DESCRIPTION   | 命令(程序)详细说明，包括选项与参数的用法 |
| AUTHOR        | 命令(程序)的作者                         |
| REPORTINGBUGS | 如果发现bug可以联系的电子邮件            |
| COPYRIGHT     | 命令(程序)版权协议                       |
| SEE ALSO      | 还可以参考哪些文档                       |

### 2.4 常用命令功能介绍

- 文件操作类常用的命令

| 命令  | 描述                        | 常用使用方法                                                 |
| ----- | --------------------------- | ------------------------------------------------------------ |
| cd    | 切换工作目录                | 进入~目录：cd 进入上次的目录：cd - 进入指定的目录：cd [directory] |
| mkdir | 创建目录                    | 创建目录：mkdir directory 创建子目录：mkdir -p parent/children 创建目录同时指定权限：mkdir -m mode directory |
| touch | 创建文件                    | 创建文件：touch file                                         |
| ls/ll | 查看目录文件                | 查看目录文件：ls 查看所有文件，包括隐藏文件：ls -a 查看并列出文件全属性：ls -l / ll 查看并列出文件全属性同时按照K/M/G显示大小：ls - lh / ll -h |
| rmdir | 删除空目录(不常用)          | 删除空目录：rmdir dir                                        |
| rm    | 删除文件                    | 删除文件：rm file 强制删除目录：rm dir -rf                   |
| ln    | 创建链接文件                | 创建硬链接：ln file1 file2 创建软链接：ln -s file1 file2     |
| cp    | 拷贝文件并重命名文件/ 目录  | 移动文件：cp file1 file2 移动目录：cp -r dir1 dir2           |
| mv    | 移动并重命名文件/目录       | 移动文件：mv file1 file2                                     |
| tar   | 压缩/解压缩文件             | 打包文件：tar -cf file.tar file 打包并调用gzip压缩文件：tar -czvf file.tar file 解压缩包：tar -xvf file.tar.gz |
| find  | 查找文件                    | 按文件名查找：find ./ -name file*                            |
| grep  | 查找字符串(后面有详细 专题) | 查找字符串：grep -rn string 查找单词：grep -rnw word         |
| cat   | 查看小文件                  | 查看文件：cat file 查看文件并显示行号：cat -n file           |
| less  | 查看大文件                  | 查看文件（空格翻页）：less file                              |
| more  | 查看大文件                  | 查看文件（空格翻页）：more file                              |
| head  | 查看文件头n行               | 查看文件头n行：head [n] file                                 |
| tail  | 查看文件尾n行               | 查看文件尾n行：tail [n] file                                 |

- 用户、权限相关操作

| 命令    | 描述                          | 常用使用方法                                                 |
| ------- | ----------------------------- | ------------------------------------------------------------ |
| sudo    | 使用超级用户权限执 行命令     | 超级用户权限执行命令：sudo cmd 切换root用户：sudo su         |
| su      | 切换用户                      | 切换用户：su user                                            |
| chmod   | 修改文件/目录权限 [r/w/x]     | 修改权限：chmod mode file 增加权限：chmod +mode file 减权限：chmod -mode file |
| chgrp   | 修改文件所属群组              | 修改群组：chgrp group file                                   |
| chown   | 修改文件拥有者                | 修改拥有者：chown owner file                                 |
| useradd | 创建用户(超级用户权 限)       | 创建用户：useradd -d /home/username -s /bin/bash -r -m username |
| userdel | 删除用户(超级用户权 限)       | 删除用户：userdel -r username                                |
| passwd  | 为用户设置密码(超级 用户权限) | 设置密码：passwd username                                    |

- 其他常用操作

| 命令     | 描述                         | 常用使用方法                                                 |
| -------- | ---------------------------- | ------------------------------------------------------------ |
| scp      | 远程拷贝文件                 | 远程拷贝文件：scp user@ip:file_path target_path 远程拷贝目录：scp -r user@ip:file_path target_path |
| reset    | 重启系统(超级用户权限)       | 重启系统：reset                                              |
| poweroff | 关机(超级用户权限)           | 关机：poweroff                                               |
| pwd      | 查看当前目录的绝对路径       | 查看路径：pwd                                                |
| wc       | 按照\n统计行数               | 统计行数：wc -l file                                         |
| ps       | 查看当前运行的进程           | 查看进程详细信息：ps -Afl                                    |
| top      | 动态监视进程(每3秒刷新 一次) | 动态监视进程：top                                            |
| kill     | 发送信号给进程               | 终止信号：kill [-15] pid 强制终止信号：kill -9 pid           |

### 2.5 环境变量的添加

环境变量添加涉及的文件
[详解/etc/profile、/etc/bash.bahsrc、~/.profile、~/.bashrc的用途](https://blog.csdn.net/jirryzhang/article/details/70833544)

- /etc/profile

  系统启动时读取

- /etc/bash.bashrc

  系统启动shell时读取

- ~/.profile
  个人用户启动时读取

- ~/.bashrc

  个人用户启动shell时读取

## 三、vim基础介绍<a name="vim基础介绍"></a>

### 3.1 vim编辑器的简介

vi是visual interface的缩写，他是EX行编辑器的可视化接口。vim是vi improved的缩写，即vi的提高版本。

以下内容来自于[vim的前世今生](https://blog.csdn.net/wangbo818/article/details/103995686)
**行编辑器ed的历史：**
Ken Thompson 创建了行编辑器
1966 年，贝尔实验室聘用了 Ken Thompson 。Thompson 刚刚在加州大学伯克利分校完成了电气工程和计算机科学的硕士学位。在伯克利他使用一个名为 QED 的文本编辑器，该编辑器在 1965 到 1966 年间被开发用于伯克利分时系统。
Thompson 在进入实验室后，Thompson 和贝尔实验室资深研究员 Dennis Ritchie（c 语言之父，Unix之父），开始怀念分时系统所提供的“交互式计算的感觉”，并着手创建他们自己的版本，该版本最终发展成为 Unix。
1969 年 8 月，Thompson “给操作系统、shell、编辑器和汇编程序分别分配了一个星期”，将新系统的基本组件组合在一起。
这个编辑器被称为 ed 。它是基于 QED 的，但并不完全是 QED 的复现。
[Ken Thompson世界上最杰出的程序员](https://baijiahao.baidu.com/s?id=1698745995338379581&wfr=spider&for=pc)
[互联网发展简史：C语言之父Dennis Ritchie](http://www.everyinch.net/index.php/internet2/)
[C语言之父丹尼斯·利奇的传奇一生](https://www.sohu.com/a/289991126_100132724)
**ed行编辑器初体验**

```shell
wen_fei@pc210525:~
$ sudo apt-get install ed
[sudo] password for wen_fei:
Reading package lists... Done
Building dependency tree
Reading state information... Done
ed is already the newest version (1.16-1).
ed set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 1 not upgraded.
wen_fei@pc210525:~
$ ed
a H
ello World
thank you
. w
1.txt
30
q w
en_fei@pc210525:~
$ vim 1.txt
```

**行编辑器ex的由来以及vi的产生**
对 Thompson 和 Ritchie 来说， ed 已经足够好了。但是其他人则认为它很难用，而且它作为一个淋漓尽致地表现 Unix 对新手敌意的例子而臭名昭著。在 1975 年，一个名叫 George Coulouris 的人在伦敦玛丽皇后学院的 Unix 系统上开发了一个改进版 ed 。当时是伯克利软件发行公司（BSD）的研究生的Bill Joy 以 Coulouris 的源代码为基础，为扩展 ed 建立了一个名为 ex 的改进版 ed。
1979 年的第 2 版 BSD 引入了一个名为 vi 的可执行文件，它只在可视模式下打开 ex 。这也就后来的Vi。
[关于Bill Joy的那些事](https://www.zhihu.com/question/19906424)
**vim的创建**
“Vim”现在是“改进版 Vi”的缩写，而最初代表的是“模拟版 Vi”。和其他许多“vi克隆版本”一样，Vim 始于在一个无法使用 vi 的平台上复现 vi 的一个尝试。在荷兰 Venlo 一家影印公司工作的软件工程师 Bram Moolenaar 想要为他全新的 Amiga 2000 准备一款类似于 vi 的编辑器。Moolenaar 已经习惯了在大学时使用的 Unix 系统上的 vi ，当时他已经对vi了如指掌。10 所以在 1988 年，Moolenaar 使用当时的STEVIE vi克隆版本开始在 Vim 上工作。

### 3.2 vim的安装

**vim的安装：**

```shell
sudo apt-get install vim
```

**vim个人插件配置**

```shell
git clone https://gitee.com/hoeprun-trainning/vim.git
cd vim/vim
bash build.sh
```

### 3.3 vim的模式

vim三种模式：normal、insert、visual
**模式切换**
normal -> visual：按v/Shift + v/Ctrl + v
insert -> normal：Esc
normal -> insert：

```shell
a append 光标后插入
i insert 光标处插入
o open a line below 光标下一行插入
A append after line 光标所在行末尾插入
I insert before line 光标所在行的行首插入
O open a line above 光标所在行的上一行插入
```

### 3.4 vim的常用操作

参考：[vim常用操作介绍](https://gitee.com/hoeprun-trainning/vim/blob/master/vimcmd.txt)

```shell
normal模式下常用命令：
保存：w
退出：q
强制退出不保存：q!
保存退出：wq/x
竖分屏：vs(vertical split)
横分屏：sp(split) 
全局替换：%s/foo/bar/g
设置行号：set number/set nu
快速跳转到最后一次编辑的地方并进入insert模式：gi
移动到下个word/WORD开头：w/W
移动到下个word/WORD结尾：e/E
移动到上个word/WORD开头：b/B
word指的是以非空白符分割的单词
WORD指的是以空白符分割的单词
f{char}：移动到char字符上
F{char}：反向移动到char字符上
t{char}：移动到char字符前一个字符
如果第一次没搜到，可以使用;/,,继续搜索该行下一个/上一个
0移动到行首第一个字符
^移动到第一个非空白字符啊(不常用)
$移动到行尾
g_移动到行尾非空白字符(不常用)
()：句子间移动(. ! ?为句子分割符)
{}：段落间移动
gg/G：移动到文件开头/结尾
Ctrl + o：快速返回
H/M/L：跳转到屏幕的开头(Head)/中间(Middle)/结尾(Lower)
Ctrl + u/f：上下翻页(upword/forward)
zz：把当前行置为屏幕中间
x：快速删除一个字符
daw/dw：快速删除一个单词包含后面的空白分隔符
diw：快速删除一个单词但不删除后面的空白分隔符
dt{char}：删除当前位置到char前的所有内容
d$：删除当前位置到行尾的所有内容
d0：删除当前位置到行首的所有内容
dd：删除当前行内容
    :e! 重新加载文件并且不保存当前内容

    vim 快速修改命令
    r(replace), c(change), s(substitute)
    r{char}：将当前光标的字符修改为char
    nr{char}:将当前光标后n个字符删除修改为n个char
    s：删除当前字符并进入插入模式
    ns：删除光标后n个字符然后进入插入模式
    c：配合文本对象可快速进行修改
    caw/cw：删除光标后整个单词(光标前的单词部分不删除)然后进入插入模式
    ct{char}：删除当前位置到char前的所有内容然后进入插入模式
    R：直接进入修改模式，同时输入的内容会替换已存在的内容，相当于overwrite模式
    S：整行删除并进入插入模式
    C：删除光标后整行的内容然后进入插入模式

    vim 替换命令(substitute)，可配合正则表达式
    :[range]s/{pattern}/{string}/[flags]
    range表示范围，比如：10, 20表示10-20行，%表示全部
    pattern是要替换的内容，string是替换后的字符串
    Flags的常用的标志:
        g(global)表示全局范围内执行
        c(confirm)表示确认，可以确认或者拒绝修改
        n(number)报告匹配到的次数而不替换，可以用来查询匹配次数

    vim 多文件操作
    vim 多文件打开
        使用vim打开一个文件后使用 :e file 可再打开文件

    Buffer Window Tab概念
        Buffer：打开的一个文件的内存缓冲区
        Window：Buffer可视化的分割区域
        Tab：组织窗口为一个工作区
    Buffer 操作：
        :ls 列举当前文件缓冲区列表，然后使用 :b n 跳转到第n个文件缓冲区
        :bpre :bnext :bfirst :blast :b buffer_name 可跳转到对应的文件缓冲区
        以上命令均可以使用Tab补全，同时跳转前当前文件缓冲区的修改必须已保存
    Window 操作：
        水平分割：<Ctrl + w>s / :sp
        垂直分割：<Ctrl + w>v / :vs
        <Ctrl + w>w 窗口间循环切换
        <Ctrl + w>h 切换到左边的窗口
        <Ctrl + w>j 切换到下边的窗口
        <Ctrl + w>k 切换到上边的窗口
        <Ctrl + w>l 切换到右边的窗口

        <Ctrl + w>H 将当前的窗口移动到左边
        <Ctrl + w>J 将当前的窗口移动到下边
        <Ctrl + w>K 将当前的窗口移动到下边
        <Ctrl + w>L 将当前的窗口移动到右边

     <Ctrl + w>n< 将当前窗口向左增大n列
    <Ctrl + w>n> 将当前窗口向右增大n列
	<Ctrl + w>= 使所有窗口等宽、等高
	<Ctrl + w>_ 最大化活动窗口的高度
	<Ctrl + w>| 最大化活动窗口的宽度
	n<Ctrl + w>_ 把活动窗口的高度设为n行
	n<Ctrl + w>| 把活动窗口的宽度设为n列
Tab 操作(用的不多)：
	:tabnew {filename} 创建一个新的标签页
	:tabe[dit] {filename} 在新标签页中打开{filename}
	<Ctrl + w>T 把当前窗口移到一个新标签页
	:tabc[lose] 关闭当前标签页及其中的所有窗口
	:tabo[nly] 只保留活动标签页，关闭所有其他标签页

	Ex命令				Normal命令		用途
	:tabn[ext] {N}		{N}gt			切换到编号为{N}的标签页
	:tabn[ext]			gt				切换到下一标签页
	:tabp[revious]		gT				切换到上一标签页
	Note:标签一般建立两个就好，太多不好操作

vim 的文本对象：
[number]<command>[text object]
number：表示次数
command：表示命令，常用的有 d(elete), c(hange), y(yank)
text object：表示要操作的文本对象，常用的有 w(ord), s(entence), p(aragraph)
iw：表示inner word. 例如viw命令表示：进入visual模式选中当前的单词(不包含单词前后的空格等)
aw：表示around word, 它不仅会选中当前的单词还会包含单词前后的空格
vi( 选中()内的内容
vi[ 选中[]内的内容
vi< 选中<>内的内容
vi{ 选中{}内的内容
vi' 选中''内的内容
vi" 选中""内的内容

复制：y(yank)
粘贴：p(put)
剪贴：d
yiw 复制一个单词
yy 复制一行

vim 寄存器使用：
"{register}前缀可以指定寄存器，默认是无名寄存器
"ayy 复制一行内容保存在寄存器a中
"ap 将寄存器a中的内容复制
vim可使用的寄存器有：
有名寄存器：a-z
复制专用寄存器："0
系统剪贴板："+
其他寄存器："% 当前文件名; ". 上次插入的文本
:set clipboard=unnamed 直接将复制的内容保存到剪切板中，粘贴时也是选择剪切板中内容

vim 的宏(macro)进行批量操作
qa 开始录制
对第一行进行相关操作后进入normal模式
q 停止录制
V 进入行选模式
G 一直选到文件末尾
:'<,'>normal @a 将第一行的操作重复在选中的行上

vim 中的补全方式
<Ctrl + n> 普通关键字补全
<Ctrl + x><Ctrl + n> 当前缓冲区关键字补全
<Ctrl + x><Ctrl + i> 包含文件关键字
<Ctrl + x><Ctrl + j> 标签文件关键字
<Ctrl + x><Ctrl + k> 字典查找
<Ctrl + x><Ctrl + l> 整行补全
<Ctrl + x><Ctrl + f> 文件名补全
<Ctrl + x><Ctrl + o> 全能(Omni)补全
常用的几种：
<Ctrl + n>/<Ctrl + p> 补全单词(可使用<Ctrl + n>/<Ctrl + p>上下选择)
<Ctrl + x><Ctrl + f> 补全文件名
<Ctrl + x><Ctrl + o> 补全代码，需要开启文件类型检查，安装插件
:r! echo % 表示插入当前文件名
:r! echo %:p 表示插入当前文件的完整路径

vim 更换配色
:colorscheme 显示当前的主题配色
:colorscheme <Ctrl + d> 显示所有的配色
:colorscheme 配色名 修改配色

vim 快速查询命令
/：向后进行搜索
?：向前进行搜索
n/N：跳转到下一个/上一个匹配
*/#：单词向后/向前匹配

visual模式下常用命令：

使用V选择行或者直接V进入visual模式
使用Ctrl + v进行方块选择或者直接V进入visual模式
u/U 将选中的文本改为全小写/大写

insert模式下常用命令：
删除上一个字符：Ctrl + h
删除上一个单词：Ctrl + w
删除当前行：Ctrl + u
```

### 3.5 vimrc的配置  

[个人vimrc配置介绍](https://gitee.com/hoeprun-trainning/vim/blob/master/vimrc.txt)

```shell
该文档描述了vim的vimrc常用快捷键的设置及插件的安装
一、设置选项
    1、可使用 :h option-list 查看有哪些选项可设置
    2、常用的选项设置(参见vimrc中的开头设置)
二、映射
几个常见名词概念：
    1> map a b 表示按下a等于按下b
    2> vmap/imap/nmap 分别表示在visual/insert/normal模式下的映射
    3> noremap/map 分别表示非递归映射/递归映射
    4> 递归映射：map a b; map c a 表示按下c等于按下a，同时按下a等于按下b，所以按下c实际是按下b
    5> 非递归映射：noremap a b; noremap c a 表示按下c就是按下a不会递归成b
    6> 任何时候都应该尽量使用非递归映射
1、设置 leader 键：用于在insert模式下快捷操作的开始按键
    let mapleader=',' 表示设置,为leader键，注意等号前后不能有空格
2、map 命令介绍
    imap = insert map 表示插入模式下的递归映射
    nmap = normal map 表示普通模式下的递归映射
    vmap = visual map 表示块选择模式下的递归映射
    inoremap = insert no recursive map 表示插入模式下非递归映射
    nnoremap = normal no recursive map 表示普通模式下非递归映射
    vnoremap = visual no recursive map 表示块选择模式下非递归映射
3、普适情况下的常用快捷映射
    map <space> viw 表示按下空格时候选中整个单词
    noremap <C-h> <C-w>h 表示normal/visual模式下按<Ctrl + h>直接切屏
    noremap <C-j> <C-w>j 表示normal/visual模式下按<Ctrl + j>直接切屏
    noremap <C-k> <C-w>k 表示normal/visual模式下按<Ctrl + k>直接切屏
    noremap <C-l> <C-w>l 表示normal/visual模式下按<Ctrl + l>直接切屏
4、insert模式下常用快捷映射
    inoremap jj <Esc> 表示按下jj等于按下<Esc>进入normal模式
    inoremap <leader>d <Esc>ddO 表示按下,d等于删除一行并另起一行进入插入模式
    inoremap <leader>w <Esc>:w<cr> 表示按下,w等于保存文本
5、normal模式下常用快捷映射
    nnoremap - x 表示按-等于按x
    nnoremap <leader>w :w<cr> 表示按下,w自动保存文件

二、vim 插件
1、常见常见种类：vim-plug, Vundle, Pathogen, Dein.Vim, volt，本配置采用 vim-plug
2、搜索插件的方法：
    1> Google搜索关键字查找想要的插件
    2> https://vimawesome.com 中搜索
3、安装 vim-plug
    1> 终端执行：curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    2> ~/.vimrc 中增加两行内容
        call plug#begin('~/.vim/plugged')
        call plug#end()
    3> 后面需要安装的插件就在上面两行内插入即可
    4> 插件的安装即将插件的GitHub地址前缀删除，后面即为插件名，具体参见vimrc
    5> 添加新插件需要打开vimrc执行 :PlugInstall
4、vim-startify 插件
    1> 介绍：vim 启动界面插件，使用 vim 打开时界面好看，同时会显示历史浏览记录
    2> 插件命令：Plug 'mhinz/vim-startify'
5、vim-airline 插件
    1> 介绍：状态栏美化插件
    2> 插件命令：Plug 'vim-airline/vim-airline'
6、indentline 插件
    1> 介绍：增加代码缩进线插件
    2> 插件命令：Plug 'yggdroot/indentline'
7、nerdtree 插件
    1> 介绍：文件目录树管理
    2> 插件命令：Plug 'scrooloose/nerdtree'
    3> 快捷映射配置：
        nnoremap <leader>v :NERDTreeFind<cr> 表示 ,v 打开NERDTree插件并找到当前文件名位置
        nnoremap <leader>g :NERDTreeToggle<cr> 表示 ,v 打开/关闭NERDTree
8、ctrlp 插件
    1> 介绍：快速查找并打开一个文件
    2> 插件命令：Plug 'ctrlpvim/ctrlp.vim'
    3> 用法：normal模式按 Ctrl + p 后进行操作
9、easymotion 插件
    1> 方便移动到任意想要的位置
    2> 插件命令：Plug 'easymotion/vim-easymotion'
10、vim-surround 插件
    1> 介绍：normal模式下增加、删除、修改成对内容
    2> 插件命令：Plug 'tpope/vim-surround'
    3> 使用命令：
        ds(delete a surrounding): ds ( 表示删除 ()
        cs(change a surrounding): cs " ' 表示将 " 修改为 '; cs ( [ 表示将 () 改为 []但是两端会加空格，因此一般使用 cs ( ]
        ys(you add a surrounding): ys iw " 表示将单词两端添加 ""
11、fzf 和 fzf.vim 插件
    1> 介绍：命令行模糊搜索工具
    2> 插件命令：
        Plug 'junegunn/fzf', { 'do': './install --bin' }
        Plug 'junegunn/fzf.vim'
    3> 使用方法：
        要想使用Ag功能需要先安装Ag：sudo apt-get install silversearcher-ag
        Ag [PATTERN] 模糊搜索字符串
        Files [PATH] 模糊搜索目录
12、far.vim 插件
    1> 介绍：多文件批量搜索和替换
    2> 插件命令：Plug 'brooth/far.vim'
    3> 使用方法：
        :Far foo bar **/*.py
        :Fardo
13、vim-go 插件
    1> 介绍：go语言最小配置
    2> 插件命令：Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
14、python-mode 插件
    1> 介绍：轻量级的 Python IDE
    2> 插件命令：Plug 'python-mode/python-mode'
15、tagbar 插件
    1> 介绍：代码大纲
    2> 插件命令：Plug 'majutsushi/tagbar'
16、interestingwords 插件
    1> 介绍：高亮感兴趣的单词
    2> 插件命令：Plug 'lfv89/vim-interestingwords'
17、deoplete.nvim 插件
    1> 介绍：代码补全
    2> 插件命令：Plug 'shougo/deoplete.nvim'
    3> Note: 必须使用的vim8才能用该插件


三、vim 配色
1、vim-hybird 配色：w0ng/vim-hybird
2、solarized 配色：altercation/vim-colors-solarized
3、gruvbox 配色：morhetz/gruvbox

Note: 我个人使用的是 warriors-away + vorange 配合的配色
```

### 3.6 vim推荐的几个网站

[vim插件官网](https://vimawesome.com/)
[vim在线中文文档](https://www.topbyte.cn/vimdoc/doc/help.html)
[vim在线练习](https://www.shortcutfoo.com/app/dojos/vim)
[vim配置及插件安装管理](https://blog.csdn.net/goodluckwhh/article/details/86675491)
[笨办法学Vimscript](https://gitee.com/hoeprun-trainning/vim/tree/master/book)

## 四、shell编程一<a name="shell编程一"></a>

### 4.1 shell概述

- **Linux体系结构**

  - **硬件操作：**硬件寄存器的控制操作

  - **Linux内核：**系统调用、文件系统管理、I/O、内存管理、进程调度、异常中断处理、cache等

  - **shell：**用户与内核之间的交互，或者叫做人机交互

  - **使用程序：**命令、程序app、shell脚本等

    ![image-20221215210028967](../img/linux_base/README/4.png)

- **shell介绍**

  - 命令是用户向系统内核发出控制请求，与之交互的文本流
  - shell是一个命令行解释器，将用户命令解析为操作系统所能理解的指令，实现用户与操作系统的交互
  - 当需要重复执行若干命令，可以将这些命令集合起来，加入一定的控制语句，编辑成为shell脚本文件，交给shell批量执行

- **shell执行流程------人机交互过程**

  - 用户在命令行提示符下键入命令文本，开始与Shell进行交互

  - shell将用户的命令或按键转化成内核所能够理解的指令

  - 控制操作系统做出响应，直到控制相关硬件设备

  - 然后，shell将输出结果通过shell提交给用户

    ![image-20221215210158710](../img/linux_base/README/5.png)

- **shell的分类**

  - **Bourne Shell（简称sh）：**它是Unix的第一个shell程序，早已成为工业标准。目前几乎所有的Linux系统都支持它。不过Bourne Shell的作业控制功能薄弱，且不支持别名与历史记录等功能
  - **C Shell（简称csh）**
  - **Korn Shell（简称ksh）**
  - **Bourne Again Shell：**能够提供环境变量以配置用户Shell环境，支持历史记录，内置算术功能，支持通配符表达式，将常用命令内置简化

- **shell学习的简单性**

  实例一：C语言实现输出a + b的值

  实例二：取5个正整数中的最小值

- **shell命令格式**

  ```shell
  Command [-Options] Argument1 Argument2 …
  指令 选项 参数1 参数2…
  $：Shell提示符，如果当前用户为超级用户，提示符为“#”，其他用户的提示符均为“$”；
  Command：命令名称，Shell命令或程序，严格区分大小写
  Options：命令选项，用于改变命令执行动作的类型，由“-”引导，可以同时带有多个选项；
  Argument：命令参数，指出命令作用的对象或目标，有的命令允许带多个参数。
  Note:
  1) 一条命令的三要素之间用空格隔开；
  2) 若将多个命令在一行书写，用分号（;）将各命令隔开；
  3) 如果一条命令不能在一行写完，在行尾使用反斜杠（\）标明该条命令未结束
  ```

### 4.2 shell的特殊符号

- **通配符**

  | 符号   | 功能                          | 用法                                                         |
  | ------ | ----------------------------- | ------------------------------------------------------------ |
  | *      | 匹配任意长度的字符串          | 用file_*.txt，匹配file_wang.txt、file_Lee.txt、 file3_Liu.txt |
  | ？     | 匹配一个长度的字符            | 用file_?.txt，匹配file_1.txt、file1_2.txt、file_3.txt        |
  | [...]  | 匹配其中指定的一个字符        | 用file_[otr].txt，匹配file_o.txt、file_r.txt和file_t.txt     |
  | [-]    | 匹配指定的一个字符范围        | 用file_[a-z].txt，匹配file_a.txt、file_b.txt，直到 file_z.txt |
  | [^...] | 除了其中指定的字符，均 可匹配 | 用file_[^otr].txt，除了file_o.txt、file_r.txt和file_t.txt 的其他文件 |

- **转义符**

  | 符号 | 功能                                                       | 用法 |
  | ---- | ---------------------------------------------------------- | ---- |
  | \n   | 回车换行符                                                 |      |
  | \r   | 回车符                                                     |      |
  | \t   | 制表符                                                     |      |
  | \v   | 竖向跳格                                                   |      |
  | \b   | 退格                                                       |      |
  | \f   | 走纸换页                                                   |      |
  | \a   | 鸣铃                                                       |      |
  | \\   | 反斜杠字符                                                 |      |
  | \'   | 单引号                                                     |      |
  | \"   | 双引号                                                     |      |
  | \ddd | 1~3 位八进制数所代表的字符， d 的值可以是 0~7 的任何数字   | \102 |
  | \xhh | 1~2 位十六进制数所代表的字符， h 的值可以是 0~f 的任何字符 | \x0A |

- **管道、重定向**

  | 符号                | 功能                                                         | 用法                             |
  | ------------------- | ------------------------------------------------------------ | -------------------------------- |
  | \|                  | 把一系列命令连接起来，前一个命令的输出将作 为后一个命令的输入 | cat /etc/passwd \| wc -l         |
  | > file              | 将file文件当作重定向输出源，会清空file内的内容               | echo -e "Hello" > log            |
  | >> file             | 将file文件当作重定向输出源，追加到已有的file内 容后面        | echo -e "Hello" > log            |
  | 2> file或者 &> file | 将命令产生的错误信息输入到文件中                             | ls noexistingfile.txt 2> err.log |
  | < file              | 将file文件重定向为输入源                                     | wc -l < file                     |

- **引号类**

  | 符号  | 功能                                                         | 用法                             |
  | ----- | ------------------------------------------------------------ | -------------------------------- |
  | ''    | 将其中的内容全部解释为普 通的字符串，并将解释后的 字符串输出，不会接卸变量 | export NAME=Hoperun echo '$NAME' |
  | ""    | 将其中的内容全部解释为普 通的字符串，并将解释后的 字符串输出，同时会解析变 量 | export NAME=Hoperun echo "$NAME" |
  | \` \` | 命 令置换，将一个命令的输出作为另一个 命令的参数，等同于$()  | \`pwd\`                          |

- **括号类**

  | 符 号 | 功能                                                         | 用法    |
  | ----- | ------------------------------------------------------------ | ------- |
  | {}    | 1) 变量范围限定：${} 2) 命令分组：不会创建子进程             |         |
  | []    | 1) 通配符：[ab] 2) 数学运算：$[$var * 5] 3) if语句中条件判断的比较：数值比较、字符串比较、文件比较等，等同 于test |         |
  | [[]]  | 模式匹配，变量与值比较时会将变量转换为数值，不会报告警       |         |
  | ()    | 1) 命令置换：$() 2) 命令分组，和主程序不属于同一个进程：(cmd1; cmd2...) 3) 数组变量：(a b c d) |         |
  | (())  | 数学表达式运算                                               | ((a++)) |

### 4.3 shell变量定义

- **位置变量**

  | 符号          | 含义                                                         |
  | ------------- | ------------------------------------------------------------ |
  | $0            | 脚本文件名                                                   |
  | $1, $2, ...$9 | 第1-9个命令行参数                                            |
  | $#            | 命令行参数个数                                               |
  | $@            | 所有的命令行参数，当被""包含时，他是被分开的                 |
  | $?            | 前一个命令退出状态                                           |
  | $*            | 所有命令行参数，当被""包含时，他是一个整理，所以一般不用这种方式 |
  | $$            | 正在执行进程的ID号                                           |

- **环境变量**

  可以通过env命令查看部分环境变量

  | 变量名   | 含义                                              |
  | -------- | ------------------------------------------------- |
  | HOME     | 用户主目录                                        |
  | IFS      | Internal Field Separator，默认是空格、tab及换行符 |
  | PATH     | shell的搜索路径                                   |
  | PS1/S2   | 默认提示符$及换行提示符>                          |
  | HISTSIZE | 保存历史记录的条目数                              |
  | LOGNAME  | 当前登录用户名                                    |
  | HOSTNAME | 主机名称                                          |
  | SHELL    | 当前使用的Shell类型                               |
  | UID      | 登录用户的ID                                      |
  | USER     | 显示当前用户名                                    |

- **自定义变量**

  export var=123

  ```shell
  Note:
  1) 变量没有数据量类型，所有的赋值均被解释为字符串
  2) 变量、等号、值之间不能有空格
  ```

### 4.4 shell的四则运算

shell运算可以通过expr、let、bc、$[]或者(())实现，一般通过shell也只是进行简单的四则运算，复杂的运算有很多其他语言可以实现，没有必要通过shell来处理。

shell本身不支持浮点运算，如果想计算浮点，可以通过bc实现，了解即可，不知道耶没关系。

- **expr**

  只支持整除，不带小数，不支持复杂的运算

  ```shell
  #!/bin/bash
  a=5
  b=4
  echo "$(expr $a + $b)"
  echo "$(expr $a - $b)"
  echo "$(expr $a \* $b)"
  echo "$(expr $a / $b)"
  echo "$(expr $a % $b)"
  # echo "$(expr (($a + $b) / ($a - $b)))"
  ```

- **let**

  和expr几乎一样，不过后面的语句需要双引号括起来，同时没有返回值
  ```shell
  #!/bin/bash
  set -e
  a=5
  b=3
  let "add = a + b"
  let "sub = a - b"
  let "mul = a * b"
  let "div = a / b"
  let "mod = a % b"
  echo -e "add = ${add}"
  echo -e "sub = ${sub}"
  echo -e "mul = ${mul}"
  echo -e "div = ${div}"
  echo -e "mod = ${mod}
  ```

- **bc**

  ```shell
  #!/bin/bash
  set -e
  a=5
  b=3
  ret=$(echo "scale=6; ${a} / ${b}" | bc)
  echo -e "${ret}"
  ```

- **$[]**  

  ```shell
  #!/bin/bash
  set -e
  a=5
  b=3
  echo "$[$a + $b]"
  echo "$[$a - $b]"
  echo "$[$a * $b]"
  echo "$[$a / $b]"
  echo "$[$a % $b]"
  echo "$[($a + $b) * ($b - $a)]"
  ```

- **(())**

  ```shell
  #!/bin/bash
  set -e
  a=5
  b=3
  echo "$(($a + $b))"
  echo "$(($a - $b))"
  echo "$(($a * $b))"
  echo "$(($a / $b))"
  echo "$(($a % $b))"
  echo "$((($a + $b) * ($b - $a)))"
  ```

### 4.5 作业

1、使用shell脚本创建用户名并配置密码，创建密码可以是一个chpasswd命令

2、查找当前目录下是否有test开头或者tess开头的文件，然后将其改为tttt开头

3、查找当前目录下是否有.bat结尾的文件，有的话把文件名（不带.bat后缀）打印出来

4、使用shell脚本实现9*9乘法口诀表

## 五、shell编程二<a name="shell编程二"></a>

### 5.1 正则表达式

[正则表达式|菜鸟教程](https://www.runoob.com/regexp/regexp-tutorial.html)

### 5.2 shell控制语句介绍

#### 5.2.1 if 条件控制语法

- **if ... fi**

  ```shell
  if condition; then
  command1
  command2
  ...
  commandN
  fi
  ```

- **if ... else ... fi**

  ```shell
  if condition; then
  command1
  command2
  ...
  commandN
  else
  command
  fi
  ```

- **if ... else ... if ... else ... fi**

  ```shell
  if condition1; then
  command1
  elif condition2; then
  command2
  else
  commandN
  fi
  ```

#### 5.2.2 case 条件控制语法

```shell
case 值 in
模式1)
command1
command2
...
commandN
;;
模式2)
command1
command2
...
commandN
;;
esac
```

#### 5.2.3 for 循环控制语法

```shell
for var in item1 item2 ... itemN
do
command1
command2
...
commandN
done
```

#### 5.2.4 while 循环控制语法

```shell
while condition
do
command
done
```

#### 5.2.5 until 循环控制语法

```shell
until condition
do
command
done
```

#### 5.2.6 break和continue

用法和C语言中的类似

### 5.3 函数

```shell
[ function ] funname [()]
{
action;
[return int;]
} N
ote：
1) 函数参数通过$1、$2...获取
2) 函数返回值通过$?接收
```

### 5.4 字符串及文件处理工具：awk、sed、grep

#### 5.4.1 awk

awk是一个强大的文本分析工具，相对于grep的查找、sed的编辑，awk在其对数据分析并生成报告时，显得尤其强大。
awk就是把文件逐行的读入，以空格(Space)或制表符(Tab)为默认分隔符将每行切片，切开的部分再进行各种分析处理。
**awk -h 查看帮助文档**
**awk语法格式**

```shell
awk [option] 'commands' filenames
awk [option] -f awk-script-file filenames
commands:
BEGIN{} {} END{}
行处理前 行处理 行处理后
```

```shell
$ ifconfig eth2 | grep eth2 | awk -F : -v OFS=',' '{print $1, $2}'
eth2, flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
```

**awk的内部变量**  

| 变量 名 | 含义                          | 示例                                                         |
| ------- | ----------------------------- | ------------------------------------------------------------ |
| $0      | 保存当前记录的内容            | cat /etc/passwd \| awk -F : '{print $0}'                     |
| NR      | 按照文件显示记录数            | awk -F : '{print NR，$1}' /etc/passwd /etc/hosts             |
| FNR     | 累加显示记录数                | awk -F : '{print FNR，$1}' /etc/passwd /etc/hosts            |
| NF      | 保存记录的字段数              | awk -F : '{print $1, NF}' /etc/passwd                        |
| FS      | 输入字段分隔符，默认 空格     | awk 'BEGIN{FS=":"} {print $1, $3}' /etc/passwd               |
| OFS     | 输出字段分隔符                | awk 'BEGIN{FS = ":"; OFS = ","} {print $1, $3}' /etc/passwd  |
| RS      | 输入记录分隔符，默认 是换行符 | awk 'BEGIN{FS=":"; OFS="+++"; RS=" "} {print $1, $3}' /etc/passwd |
| ORS     | 输出记录分隔符，默认 是换行符 | awk 'BEGIN{FS=":"; OFS="+++"; RS=" "; ORS=" "} {print $1, $3}' /etc/passwd |

**作业一：**
使用awk将文件所有行内容合并成一行

## 六、Makefile基础知识讲解<a name="Makefile基础知识讲解"></a>

参考文档：[Makefile基础介绍](./Makefile/MakefileProgrammer.md)

## 七、Git实操介绍<a name="Git实操介绍"></a>

### 7.1 Git的安装

- Linux下的安装方式：sudo apt-get install git
- windows下的安装：[git官网](https://git-scm.com/)

### 7.2 Git 常用的配置

```shell
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global core.editor vim
git config --global credential.helper store
```

### 7.3 ssh配置

- **检查是否已经有ssh key，如果有直接跳到第三步或者直接删除**

  ```shell
  ls -a ~/.ssh
  ```

- **生成新的ssh key**

  输入以下命令一路回车即可

  ```shell
  ssh-keygen -t rsa -C "myemail@example.com"
  ```

- **将ssh key添加到Gitee凭证中**

  打开文件`~/.ssh/id_rsa.pub`拷贝到如下位置[SSH公钥配置](https://gitee.com/profile/sshkeys)

  ![image-20221215232137573](../img/linux_base/README/6.png)

### 7.4 Git常用命令
