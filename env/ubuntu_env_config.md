# Ubuntu环境搭建

## 一、代码更新源替换

- 该更新源仅适用于Ubuntu20.04
- wsl的配置参见：[windows的wsl配置](./wsl_env_config.md)

```shell
sudo su
cd /etc/apt/
mv sources.list sources.list_bak
vim sources.list
添加以下内容到sources.list中

deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
```

```
apt-get update
apt-get upgrade
```

## 二、root权限安装的工具包

一键自动化配置root依赖和环境，root账号下执行如下命令即可，手动安装参考下面的说明

```shell
apt-get install git
git clone https://gitee.com/open-riscv/openharmony_env_init.git
cd openharmony_env_init
bash root_init.sh
```

### 2.1 直接安装的程序和依赖包(必须一个一个安装)

```shell
apt-get -f -y install apt-utils
apt-get -f -y install vim
apt-get -f -y install software-properties-common
apt-get -f -y install openssh-server
apt-get -f -y install iputils-ping
apt-get -f -y install curl
apt-get -f -y install net-tools
apt-get -f -y install bsdmainutils
apt-get -f -y install kmod
apt-get -f -y install bc
apt-get -f -y install rsync
apt-get -f -y install gawk
apt-get -f -y install ssh
apt-get -f -y install ccache
apt-get -f -y install zip
apt-get -f -y install python-dev
apt-get -f -y install make
apt-get -f -y install m4
apt-get -f -y install gcc-multilib
apt-get -f -y install ca-certificates-java
apt-get -f -y install unzip
apt-get -f -y install python3-yaml
apt-get -f -y install perl
apt-get -f -y install openssl
apt-get -f -y install libssl1.1
apt-get -f -y install gnupg
apt-get -f -y install xsltproc
apt-get -f -y install x11proto-core-dev
apt-get -f -y install tcl
apt-get -f -y install python3-crypto
apt-get -f -y install python-crypto
apt-get -f -y install libxml2-utils
apt-get -f -y install libxml2-dev
apt-get -f -y install libx11-dev
apt-get -f -y install libssl-dev
apt-get -f -y install libgl1-mesa-dev
apt-get -f -y install lib32z1-dev
apt-get -f -y install lib32ncurses5-dev
apt-get -f -y install g++-multilib
apt-get -f -y install flex
apt-get -f -y install bison
apt-get -f -y install doxygen
apt-get -f -y install git
apt-get -f -y install subversion
apt-get -f -y install tofrodos
apt-get -f -y install pigz
apt-get -f -y install expect
apt-get -f -y install python3-xlrd 
apt-get -f -y install git-core
apt-get -f -y install gperf 
apt-get -f -y install build-essential
apt-get -f -y install zlib1g-dev
apt-get -f -y install libc6-dev-i386
apt-get -f -y install lib32z-dev
apt-get -f -y install openjdk-8-jdk
apt-get -f -y install ruby
apt-get -f -y install mtools
apt-get -f -y install python3-pip
apt-get -f -y install gcc-arm-linux-gnueabi
apt-get -f -y install genext2fs
apt-get -f -y install liblz4-tool
apt-get -f -y install libssl-dev
apt-get -f -y install autoconf
apt-get -f -y install pkg-config
apt-get -f -y install zlib1g-dev
apt-get -f -y install libglib2.0-dev
apt-get -f -y install libmount-dev
apt-get -f -y install libpixman-1-dev
apt-get -f -y install libncurses5-dev
apt-get -f -y install exuberant-ctags
apt-get -f -y install silversearcher-ag
apt-get -f -y install libtinfo5
apt-get -f -y install device-tree-compiler
apt-get -f -y install libssl-dev
apt-get -f -y install libelf-dev
apt-get -f -y install dwarves
apt-get -f -y install gcc-arm-none-eabi
apt-get -f -y install default-jdk
apt-get -f -y install u-boot-tools
apt-get -f -y install mtd-utils
apt-get -f -y install scons
apt-get -f -y install automake
apt-get -f -y install libtinfo5
apt-get -f -y install gcc-multilib
apt-get -f -y install libtool
apt-get -f -y install libgmp-dev
apt-get -f -y install texinfo
apt-get -f -y install mpc
apt-get -f -y install autotools-dev
apt-get -f -y install libmpc-dev
apt-get -f -y install libmpfr-dev
apt-get -f -y install libgmp-dev
apt-get -f -y install patchutils
apt-get -f -y install libexpat-dev
apt-get -f -y install libfdt-dev
apt-get -f -y install libncursesw5-dev
apt-get -f -y install cmake
apt-get -f -y install wget
apt-get -f -y install libelf-devel
```

### 2.2 配置sh为bash

```shell
ls -l /bin/sh           		#如果显示为“/bin/sh -> bash”则为正常，否则请按以下方式修改： 
sudo dpkg-reconfigure dash   	#然后选择no
```

### 2.3 git lfs安装

```shell
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
apt-get install git-lfs
apt install git-lfs
git lfs install
```

### 2.4 repo工具安装

```shell
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
chmod a+x repo
mv repo /usr/local/bin/
```

## 三、个人账号必须配置的工具

### 3.1 Git配置

```shell
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global core.editor vim
git config --global credential.helper store
```

### 3.3 安装hb命令

```shell
# 方法一：
# 下载一套master分支的代码然后进入root目录下执行
pip3 install build/lite
vim ~/.bashrc
export PATH=$PATH:~/.local/bin
source ~/.bashrc

# 方法二：
python3 -m pip install --user ohos-build
vim ~/.bashrc
export PATH=$PATH:~/.local/bin
source ~/.bashrc

# 方法三：
# 安装指定版本，君正使用的是0.4.3
python3 -m pip install ohos-build==0.4.3
vim ~/.bashrc
export PATH=$PATH:~/.local/bin
source ~/.bashrc
```

### 3.4 卸载hb命令

```shell
pip3 uninstall ohos-build
```

