# Win10电脑安装配置WSL指导

## 一、准备工作

1.请确认windows的c盘空间充足， 建议c盘至少具有50G以上的空余空间

2.必须运行 Windows 10 版本 2004 及更高版本（内部版本 19041 及更高版本）或 Windows 11

若要检查 Windows 版本及内部版本号，选择 Windows 徽标键 + R，然后键入“winver”，选择“确定”  。 

## 二、wsl升级到wsl2

### 2.1 查看wsl的版本

​		因最新的rk3568的编译必须基于wsl2才可以，wsl编译会阻塞直接不编译，所以如果电脑的版本是wsl的必须升级到wsl2，如果系统是win11则直接是wsl2。注意这里的wsl或者wsl2指的是在wsl中安装的Ubuntu所对应的版本，可通过以下命令查看：

​		命令wsl -l -v 是命令的缩写，后面VERSION即是所对应的版本，如果对应的版本是1则需要升级，如果是2则不需要升级。

```shell
C:\Users\wen_fei>wsl --list --verbose
  NAME            STATE           VERSION
* Ubuntu-20.04    Running         1

C:\Users\wen_fei>wsl -l -v
  NAME            STATE           VERSION
* Ubuntu-20.04    Running         1
```

### 2.2 升级wsl2

- **以管理员身份打开 PowerShell或者cmd窗口，然后输入以下命令并重启计算机：**

  （“开始”菜单 >“PowerShell”> 单击右键 >“以管理员身份运行”）

```
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

​		其中，第一行代码为启用“适用于 Linux 的 Windows 子系统”可选功能，第二行代码为启用“虚拟机平台”可选功能，该步骤为安装wsl2必须，若不希望安装wsl2则不用输入第二行代码

- **下载Linux内核更新包并安装**

​		链接：[适用于x64计算机的WSL2 Linux内核更新包](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)

- **打开 PowerShell或者cmd窗口，输入以下命令，将 WSL 2 设置为默认版本：**

  这里的设置表示后面基于wsl安装的系统（一般指Linux系统）都是wsl2

  **Note：本项设置只能使安装新的 Linux 发行版时将默认版本设置为WSL2，而不能更改已经安装的Linux分支**

```
wsl --set-default-version 2
```



若希望将WSL1设置为默认版本则输入命令`wsl --set-version`。

- **安装Linux分支**

  打开 Microsoft Store搜索Linux并选择需要的分支，此处建议选择Ubuntu 20.04 LTS，并点击获取

- **安装完成**

  首次启动新安装的 Linux 分发版时，将打开一个控制台窗口，系统会要求你等待一分钟或两分钟，以便文件解压缩并存储到电脑上。 未来的所有启动时间应不到一秒

  然后，需要为新的 Linux 分发版创建用户帐户和密码

- **修改已经安装的基于wsl的Linux发行版为wsl2**

  使用命令：wsl --set-version Ubuntu-20.04 2，这个过程会根据你该系统当前使用情况，越大时间越长，可能会持续几个小时，最好晚上操作。

  操作之前需要使用命令：wsl -t Ubuntu-20.04先关闭Linux发行版

  ```shell
  C:\Users\wen_fei>wsl -l -v
    NAME            STATE           VERSION
  * Ubuntu-20.04    Running         1
  
  C:\Users\wen_fei>wsl -t Ubuntu-20.04
  
  C:\Users\wen_fei>wsl -l -v
    NAME            STATE           VERSION
  * Ubuntu-20.04    Stopped         1
  
  C:\Users\wen_fei>wsl --set-version Ubuntu-20.04 2
  正在进行转换，这可能需要几分钟时间...
  有关与 WSL 2 的主要区别的信息，请访问 https://aka.ms/wsl2
  转换完成。
  
  C:\Users\wen_fei>wsl -l -v
    NAME            STATE           VERSION
  * Ubuntu-20.04    Running         2
  ```

## 三、wsl安装Ubuntu

### 3.1 准备工作

- **控制面板--->程序和功能--->启用或关闭Windows功能**

  - 将Hyper-V全部勾选上

  - 将适用于Linux的Windows子系统勾选上

- **下载Ubuntu20.04**

  打开Microsoft Store，输入Ubuntu，找到Ubuntu 20.04.4 LTS，点击获取安装即可

### 3.2 wsl修改默认安装路径<a name="wsl修改默认安装路径"></a>

​	如果C盘空间足够大（1T及以上）可以不用考虑该步骤

- **关闭所有的Linux发行版本**

  ```shell
  wsl --shutdown
  ```
  
- **wsl查看已安装的linux发行版本**

  ```shell
  C:\Users\wen_fei> wsl -l -v
    NAME              STATE           VERSION
  * Ubuntu-20.04      Stopped         2
  ```

- **导出已安装的发行版本**

  ```shell
  C:\Users\wen_fei> wsl --export Ubuntu-20.04 D:\wsl\Ubuntu20.04.tar
  
  将已经安装的Ubuntu-20.04打包成Ubuntu20.04.tar，注意磁盘分区空间应充足
  ```

- **注销已导出的发行版本**

  ```shell
  C:\Users\wen_fei> wsl --unregister Ubuntu-20.04
  ```

- **重新安装已导出的发行版本**

  ```shell
  C:\Users\wen_fei> wsl --import Ubuntu-20.04 D:\wsl\ubuntu20.04 D:\wsl\ubuntu20.04.tar
  
  wsl --import <安装发行版本名称> <安装发行版本路径> <已导出的发行版本>
  ```

- **删除已导出的发行版本**

  ```shell
  删除 D:\wsl\ubuntu20.04.tar
  ```

### 3.3 配置Ubuntu

- 设置wsl的Ubuntu默认登录账号

  方法一：

  打开Ubuntu的/etc/wsl.conf文件，没有就新建，写入如下内容后重新打开即可

  ```shell
  [user]
  default=username
  ```

  方法二：

  ```shell
  #如果是Ubuntu20.04：
  ubuntu2004.exe config --default-user username
  #如果是Ubuntu18.04：
  ubuntu1804.exe config --default-user username
  #如果是debian：
  debian.exe config --default-user username
  ```

- **Ubuntu环境配置**

  参考文档进行环境搭建：[Ubuntu环境配置](./Ubuntu环境配置.md)

### 3.4 wsl扩容<a name="wsl扩容"></a>

WSL 2 使用虚拟硬盘 (VHD) 来存储 Linux 文件。 在 WSL 2 中，VHD 在 Windows 硬盘驱动器上表示为 vhdx 文件。

WSL 2 VHD 使用 ext4 文件系统。 此 VHD 会自动调整大小以满足你的存储需求，并且其最大大小为 256GB。 如果 Linux 文件所需的存储空间超过此大小，则可能需要将其展开。 如果你的分发版大小增长到大于 256GB，则会显示错误，指出磁盘空间不足。 可以通过扩展 VHD 大小来纠正此错误。

**【NOTE】**：扩容的前提条件是电脑上wsl必须是wsl2，否则不能扩容。

下面介绍扩容的步骤：

- **关闭所有的Linux发行版本**

  ```shell
  wsl --shutdown
  ```

- **查找ext4.vhdx文件路径**

  - 直接进入以下路径，`CanonicalGroupLimited.Ubuntu20.04onWindows_79rhkp1fndgsc`可能有所不同

    ```shell
    C:\Users\wenfei\AppData\Local\Packages\CanonicalGroupLimited.Ubuntu20.04onWindows_79rhkp1fndgsc\\LocalState\ext4.vhdx
    ```

  - 通过**Everything软件**搜索ext4.vhdx名称找到该文件路径

- **以管理员权限打开cmd/PowerShell，执行如下命令进行扩容**

  ```shell
  diskpart
  Select vdisk file="DISK_PATH"
  detail vdisk
  expand vdisk maximum=<sizeInMegaBytes>
  exit
  
  # 个人操作方法
  PS C:\WINDOWS\system32> diskpart
  
  Microsoft DiskPart 版本 10.0.22000.653
  
  Copyright (C) Microsoft Corporation.
  在计算机上: DESKTOP-JGDI2N8
  
  DISKPART> Select vdisk file="D:\wsl\ubuntu20\ext4.vhdx"
  
  DiskPart 已成功选择虚拟磁盘文件。
  
  DISKPART> detail vdisk
  
  设备类型 ID: 0 (未知)
  供应商 ID: {00000000-0000-0000-0000-000000000000} (未知)
  状态: 已添加
  虚拟大小:  256 GB
  物理大小:  248 GB
  文件名: D:\wsl\ubuntu20\ext4.vhdx
  为子级: 否
  父文件名:
  找不到关联的磁盘号。
  
  DISKPART> expand vdisk maximum=512000  # 注意这里的单位是MB
  
    100 百分比已完成
  
  DiskPart 已成功扩展虚拟磁盘文件。
  
  DISKPART> exit
  
  退出 DiskPart...
  ```

- **打开虚拟机Ubuntu进行重新挂载**

  - 确认Ubuntu挂载的磁盘，在Ubuntu的shell中执行如下命令

    ```shell
    sudo mount -t devtmpfs none /dev
    mount | grep ext4
    
    # 个人操作方法
    wen_fei@DESKTOP-JGDI2N8:~
    $ sudo mount -t devtmpfs none /dev
    [sudo] password for wen_fei:
    mount: /dev: none already mounted on /mnt/wsl.
    wen_fei@DESKTOP-JGDI2N8:~
    $ mount | grep ext4
    /dev/sdb on / type ext4 (rw,relatime,discard,errors=remount-ro,data=ordered)
    
    # 确定挂载的磁盘是sdb
    ```

  - 重新设置大小

    ```shell
    sudo resize2fs /dev/sdb
     
    # 个人操作方法
    wen_fei@DESKTOP-JGDI2N8:~
    $ sudo resize2fs /dev/sdb
    [sudo] password for wen_fei:
    resize2fs 1.45.5 (07-Jan-2020)
    Filesystem at /dev/sdb is mounted on /; on-line resizing required
    old_desc_blocks = 32, new_desc_blocks = 63
    The filesystem on /dev/sdb is now 131072000 (4k) blocks long.
    ```

  - 至此扩容成功

### 3.5 重新挂载wsl2的磁盘ext4.vhdx

有时候我们因为系统问题，或者操作不当导致我们无法通过Ubuntu2004.exe或者wsl等启动虚拟机，但是虚拟机对应的磁盘ext4.vhdx文件还在，这时我们想要将磁盘重新与Ubuntu2004.exe关联上的操作方法如下：

- **重新安装Ubuntu虚拟机**

  在搜索窗口找到`Ubuntu 20.04 on Windows`程序，然后点击运行，或者通过命令`wsl --install -d ubuntu-20.04`命令安装，安装成功后会在安装目录下生成文件ext4.vhdx

- **执行命令`wsl --shutdown`关闭虚拟机**

- **进入上面的安装目录，然后将ext4.vhdx文件删除**

  找到安装目录的方法很多种，可自行网上搜索，最快的方式是通过**Everything软件**直接搜索ext4.vhdx就能找到

- **将自己的ext4.vhdx拷贝到上面的安装目录下**

  **【Note】：**注意自己的Linux版本一定要和安装的Linux发行版本一致

- **此时重新打开虚拟机即是自己的磁盘内容了**

- **如果需要迁移路径，可参考[3.2 wsl修改默认安装路径](#wsl修改默认安装路径)**

- **安装完成的最大空间是默认的256G，如果想扩容请参考[3.4 wsl扩容](#wsl扩容)**

## 四、常见问题与解决方法

### 4.1 在Microsoft Store下载时需要登陆账号但无法登录

可以尝试将连接外网的适配器属性中更改Internet协议版本4首选DNS服务器为4.2.2.2，成功登陆后再改回自动获得即可

若以上尝试失败，可考虑在以下链接下载并安装

[Ubuntu 20.04][https://aka.ms/wslubuntu2004]

如果愿意，你也可通过命令行下载首选的发行版。 例如，下载 Ubuntu 20.04：

```
Invoke-WebRequest -Uri https://aka.ms/wslubuntu2004 -OutFile Ubuntu.appx -UseBasicParsing
```

### 4.2 为什么要使用WSL2

对本项目而言，使用WSL1进行编译时会在某些地方卡住相当一段时间，虽然最终编译通过了，但用时很久，而WSL2没有这个问题，故更推荐WSL2。

### 4.3 其他问题参考官网的问题列表

请参阅[疑难解答](https://docs.microsoft.com/zh-cn/windows/wsl/troubleshooting#installation-issues)

## 五、wsl常用的命令

```shell
wsl -l：查看有电脑上有多少个运行的wsl
wsl -t xxxx：关闭执行的wsl
wsl --unregister xxxx：注销指定的wsl
wsl --terminate xxxx: 和wsl -t命令一样

where xxxx：xxxx命令在那个目录下
```

