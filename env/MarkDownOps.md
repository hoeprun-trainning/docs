# MarkDown文档编辑指导

## 一、为什么推荐md文档？

- Gitee上可以直接浏览查看不用下载下来，相比较word、pdf方便很多
- md可以插入图片、代码块、超链接等，相比于纯文本文件功能强大很多
- md的代码块功能比word直接插入代码好看美观很多

## 二、编写md文档推荐工具

- 编辑md文档的工具很多，这里推荐的是typora工具，个人感觉很清爽，用的很舒服
- 下载链接：
  - 官网：[Typora 官方中文站 (typoraio.cn)](https://typoraio.cn/#)
  - svn：svn://10.20.24.184/tools/装机常用软件

## 三、Typora语法介绍

### 3.1 段落/标题设置

​		typora共支持6级标题，操作方法：

- Ctrl + 标题级别，如：Ctrl + 2
- 直接右键--->段落--->选择x级标题

### 3.2 超链接设置

- 英文中括号[]和小括号()配合，[]内放入的是超链接的名称，()内放入的是超链接地址

- ()内的超链接地址可以是网上文件链接、网址、相对路径的文件、本文档内的标题等

  - 网上文件链接：[图片](https://gitee.com/halley5/documents/blob/master/img/img/wps51E.tmp.jpg)

    ```
    [图片](https://gitee.com/halley5/documents/blob/master/img/img/wps51E.tmp.jpg)
    ```

  - 网址链接：[百度一下，你就知道](https://www.baidu.com)

    ```
    [百度一下，你就知道](https://www.baidu.com)
    ```

  - 文件链接：[常用开发工具的使用](./常用开发工具的使用.md)

    文件路径必须是相对路径

    ```
    [常用开发工具的使用](./常用开发工具的使用.md)
    ```

  - 本文档内的标签跳转，在网页上依然有效，推荐使用：[文本操作](#tag_name)

    在要设置跳转的地方设置标签tag，然后在跳转的地方使用超链接
    
    ```html
    标签tag语法一：目标位置后面插入(推荐使用)，使用name属性
    <a name="tag_name"></a>
    超链接语法
    [文本操作](#tag_name)
    
    标签tag语法二：目标位置后面插入(推荐使用)，使用id属性
    <a id=1></a>
    超链接语法
    [文本操作](#1)
    
    标签tag语法三：目标位置上一行插入
    <div name="tag_name"></div>
    超链接语法
    [文本操作](#tag_name)
    ```
    
  - 本文档内的标题：[Typora语法介绍](#三、Typora语法介绍)

    ```
    [Typora语法介绍](#三、Typora语法介绍)
    ```

    **Note: 小括号内的内容是#+标题行全部内容**

- 打开超链接的方法：Ctrl + 鼠标点击超链接文字

<div name="tag_name"></div>

### 3.3 文本操作

- 加粗

  - 加粗内容两端各加两个*
  - Ctrl + B
  - 右键--->B

- 倾斜

  - 倾斜内容两端各加1个*
  - Ctrl + I
  - 右键--->I

- 下划线

  - Ctrl+ U
  - 格式--->下划线

- 注释

  - 格式--->注释

- 代码

  文本中突然插入一句代码的实现

  - 代码内容两端各加1个`
  - Ctrl + Shift + `
  - 右键---></>

- 上标签

  - ```
    <sup>xxx</sup>
    ```
  
- 缩进/减少缩进

  - Tab/Shift + Tab

### 3.4 插入操作

- 图像

  - 右键--->插入--->图像

- 表格

  - Ctrl + T
  - 右键--->插入--->表格

- 代码块

  - Ctrl + Shift + K
  - 右键--->插入--->代码块

- 内容目录

  - 右键--->插入--->内容目录


## 四、插件实用

### 4.1 设置代码块的默认编程语言

- 下载[ahk](https://autohotkey.com/download/ahk-install.exe)
- 新建一个code.ahk文件，内容如下

```shell
#IfWinActive ahk_exe Typora.exe
{
    ; Ctrl+Alt+K shell
    ; crtl是  ^ , shift是 + , k是  k键
    ^+k::addCodeJava()
}
addCodeJava(){
Send,{```}
Send,{```}
Send,{```}
Send,shell
Send,{Enter}
Return
}
```

- 双击运行code.ahk

- 在Typora软件内，在**英文输入法**状态下，实用快捷键`Ctrl + Shift + K`，即可实现默认代码类型设置

- 这是开机启动方法

  按下`Win + R`，输入`shell:Common Startup`，将刚刚保存的文件放到打开的启动文件夹内即可！

- **注意事项**

  如果想启用`Ctrl + Shift + K`功能，则不能使用`Ctrl + Shift`来控制输入法切换，否则会冲突
