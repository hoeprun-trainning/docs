#  润和培训文档管理

# 目录

- [Linux基础知识](#Linux基础知识)
- [C语言知识](#C语言知识)
- [C++语言知识](#C++语言知识)
- [鸿蒙知识](#鸿蒙知识)
- [基础工具使用](#基础工具使用)

## 一、Linux基础知识<a name="Linux基础知识"></a>

- [Linux基础知识培训](./linux_base/README.md)

- [Makefile学习指导](./linux_base/Makefile/MakefileProgrammer.md)

## 二、C语言知识<a name="C语言知识"></a>

## 三、C++语言知识<a name="C++语言知识"></a>

## 四、鸿蒙知识<a name="鸿蒙知识"></a>

## 五、基础工具使用<a name="基础工具使用"></a>

- [Win10电脑安装配置WSL指导](./env/wsl_env_config.md)
- [Ubuntu基础环境配置](./env/ubuntu_env_config.md)
- [MarkDown文档编辑指导](./env/MarkDownOps.md)
