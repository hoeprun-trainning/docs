# 进程编程

## 一、进程的概念

### 1.1 什么是程序

- 存放在磁盘上的指令和数据的有序集合（文件）
- 静态的

### 1.2 什么是进程

- 执行一个程序所分配的资源的总称
- 进程是程序的一次执行过程
- 动态的，包括创建、调度、执行和消亡

### 1.3 进程的内容

进程包含正文段、用户数据段和系统数据段，其中正文段和用户数据段属于程序的。

系统数据段包含：进程控制块、CPU寄存器值、堆栈

- **进程控制块**
  - 进程标识PID
  - 进程用户
  - 进程状态、优先级
  - 文件描述符表

### 1.4 进程类型

交互进程：在shell下启动。在前台运行、也可以在后台运行

批处理进程：和终端无关，被提交到一个作业队列中以便顺序执行

守护进程：和终端无关，一直在后台运行

### 1.5 进程状态

运行态：进程正在运行，或者准备运行

等待态：进程在等待一个事件的发生或某种系统资源

停止态：进程被中止，收到信号后可继续运行

死亡态：已终止的进程，但pcb没有被释放

![附图2：进程状态转换关系](../img/process_thead/process_programming/0.png)

## 二、进程的操作

### 2.1 查看进程信息的方法

- **ps：查看系统进程的快照**

  **ps -ef**

  ![image-20220620091440942](../img/process_thead/process_programming/1.png)

  **ps -aux**

  ![image-20220620091607441](../img/process_thead/process_programming/2.png)

- **top：查看进程动态信息**

- **/proc：查看进程详细信息**

- **nice：按用户指定的优先级运行进程**

  优先级：-20-+19；-20最高优先级

  eg：nice -n 2 a.out

- **renice：改变正在运行进程的优先级**

  普通用户只能够降低优先级不能提高优先级，管理员权限可以提高也能降低

  renice -n 3 pid

- **jobs 查看后台进程**
- **bg 将挂起的进程在后台运行**
- **fg 把后台运行的进程放到前台运行**

## 三、进程的创建和推出

### 3.1 fork创建进程

```c
#include <sys/types.h>
#include <unistd.h>
pid_t fork(void);
```

创建新的进程，失败返回-1；

成功时候父进程返回子进程的进程号，子进程返回0

通过fork的返回值区分父进程和子进程

```c
pid_t pid;
if ((pid = fork()) < 0) {
    printf("fork failed!\n");
    return -1;
} else if (pid == 0) {
    printf("child process: my pid is %d\n", getpid());
} else {
    printf("parent process: my pid is %d\n", getpid());
    printf("child pid is %d\n", pid);
}
```

### 3.2 父子进程

- 子进程继承了父进程的内容
- 父子进程有独立的地址空间，互不影响
- 若父进程先结束
  - 子进程成为了孤儿进程，被init进程收养
  - 子进程编程后台进程
- 若子进程先结束
  - 父进程如果没有及时回收，子进程编程僵尸进程

### 3.3 进程结束：exit/_exit

```c
#include <stdlib.h>
void exit(int status);
#include <unistd.h>
void _exit(int status);
```

status	表示让进程结束时的状态，默认使用0表示正常结束

exit结束进程时会刷新流缓冲区

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Hello World");
    exit(0);
    printf("Hello World");
}
```

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Hello World");
    _exit(0);
    printf("Hello World");
}
```

## 四、exec函数族

如果我们使用fork()函数创建一个子进程，则该子进程几乎复制了父进程的全部内容，也就是说，子进程与父进程在执行同一个可执行程序。那么我们能否让子进程不执行父进程正在执行的程序呢？

exec函数族提供了让进程运行另一个程序的方法。exec函数族内的函数可以根据指定的文件名或目录名找到可执行程序，并加载新的可执行程序，替换掉旧的代码区、数据区、堆区、栈区与其他系统资源。这里的可执行程序既可以是二进制文件，也可以是脚本文件。在执行exec函数族函数后，除了该进程的进程号PID，其他内容都被替换了。
通常情况下，我们首先使用fork()函数创建一个子进程，然后调用exec函数族内函数将子进程内程序替换成其他的可执行程序，这样看起来就像父进程诞生了一个新的且完全不同于父进程的子进程。

exec函数族有6个函数，这些函数的函数名、函数功能、函数参数列表有相似之处，我们在使用的过程中一定要仔细区分这些函数的区别避免混淆。有关exec函数族的更多使用方法内容请查阅man手册。

```c
#include <unistd.h>
extern char **environ;
int execl(const char *pathname, const char *arg, ...);
int execlp(const char *file, const char *arg, ...);
int execle(const char *pathname, const char *arg, ...);
int execv(const char *pathname, char *const argv[]);
int execvp(const char *file, char *const argv[]);
int execvpe(const char *file, char *const argv[], char *const envp[]);
```

这6个函数的函数功能类似，但是在使用语法规则上有细微区别。我们可以看出，其实exec函数族的函数都是exec+后缀来命名的，具体的区别如下：
区别1：参数传递方式（函数名含有l还是v）
exec函数族的函数传参方式有两种：逐个列举或指针数组。
    若函数名内含有字母'l'（表示单词list），则表示该函数是以逐个列举的方式传参，每个成员使用逗号分隔，其类型为const char *arg，成员参数列表使用NULL结尾
    若函数名内含有字母'v'（表示单词vector），则表示该函数是以指针数组的方式传参，其类型为char *const argv[]，命令参数列表使用NULL结尾
区别2：查找可执行文件方式（函数名是否有p）
我们可以看到这几个函数的形参有些为path，而有些为file。其中：
    若函数名内没有字母'p'，则形参为path，表示我们在调用该函数时需要提供可执行程序的完整路径信息
    若函数名内含有字母'p'，则形参为file，表示我们在调用该函数时只需给出文件名，系统会自动按照环境变量$PATH的内容来寻找可执行程序
区别3：是否指定环境变量（函数名是否有e）
exec可以使用默认的环境变量，也可以给函数传入具体的环境变量。其中：
    若函数名内没有字母'e'，则使用系统当前环境变量
    若函数名内含有字母'e'（表示单词environment），则可以通过形参envp[]传入当前进程使用的环境变量
exec函数族简单命名规则如下：
    后缀    能力
    l        接收以逗号为分隔的参数列表，列表以NULL作为结束标志
    v        接收一个以NULL结尾的字符串数组的指针
    p        提供文件的完整的路径信息 或 通过$PATH查找文件
    e        使用系统当前环境变量 或 通过envp[]传递新的环境变量
这6个exec函数族的函数，execve()函数属于系统调用函数，其余5个函数属于库函数。

例如：

```c
// execl()
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
    pid_t pid;
    pid = fork();
    if (pid==-1) {
        perror("cannot fork");
        return -1;
    } else if (pid==0) {
        printf("This is Child process\n");
        if(execl("/bin/ps","ps","-ef",NULL)<0) { //子进程执行ps -ef，注意参数的写法，且需要使用NULL结尾
            perror("cannot exec ps");
        }
    } else {
        printf("This is Parent process\n");
        sleep(1);//父进程延时1s，让子进程先运行
    }
    return 0;
}

// execlp()
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
    pid_t pid = fork();
    if (pid==-1) {
        perror("cannot fork");
        return -1;
    } else if(pid==0) {
        printf("This is Child process\n");
        if(execlp("ps","ps","-ef",NULL)<0) { //第一个参数只需要写ps即可，系统会根据环境变量自行寻找ps程序的位置
            perror("cannot exec ps");
        }
    } else {
        printf("This is Parent process\n");
        sleep(1);//父进程延时1s，让子进程先运行
    }
    return 0;
}

// execvp()
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
    pid_t pid;
    char *arg[]={"ps","-ef",NULL}; //设定参数向量表，注意使用NULL结尾
    pid = fork();
    if (pid==-1) {
        perror("cannot fork");
        return -1;
    } else if(pid==0) {
        printf("This is Child process\n");
        if(execvp("ps",arg)<0) { //注意该函数的参数与execlp()函数的区别
            perror("cannot exec ps");
        }
    } else {
        printf("This is Parent process\n");
        sleep(1);
    }
    return 0;
}

// execle()
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
	pid_t pid;
	char *envp[]={"PATH=/tmp","USER=liyuge",NULL}; //设定新的环境变量，注意使用NULL结尾
	pid = fork();
	if (pid==-1) {
		perror("cannot fork");
		return -1;
	} else if(pid==0) {
		printf("This is Child process\n");
		if(execle("/usr/bin/env","env",NULL,envp)<0) {
			perror("cannot exec env");
		}
	} else {
		printf("This is Parent process\n");
		sleep(1);//父进程延时1s，让子进程先运行
	}
	return 0;
}

// execve()
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
	pid_t pid;
	char *arg[]={"env",NULL};//设定参数向量表，注意使用NULL结尾
	char *envp[]={"PATH=/tmp","USER=liyuge",NULL};//设定新的环境变量，注意使用NULL结尾
	pid = fork();
	if (pid==-1) {
		perror("cannot fork");
		return -1;
	} else if(pid==0) {
		printf("This is Child process\n");
		if(execve("/usr/bin/env",arg,envp)<0)
		{
			perror("cannot exec env");
		}
	} else {
		printf("This is Parent process\n");
		sleep(1);//父进程延时1s，让子进程先运行
	}
	return 0;
}
```

## 五、system函数

```c
#include <stdlib.h>
int system(const char *command);
```

成功返回命令command的返回值，失败返回EOF

```c
#include <stdlib.h>

int main(int argc, char *argv[])
{
    system("ls -l");
    return 0;
}
```

## 六、进程回收

子进程结束时由父进程回收

孤儿进程由init进程回收

若没有及时回收会出现僵尸进程

### 6.1 wait函数

```c
#include <unistd.h>
pid_t wait(int *status);
```

成功时返回回收的子进程的进程号；失败时返回EOF

若子进程没有结束，父进程一直阻塞

若有多个子进程，哪个先结束就先回收

status指定保存子进程返回值和结束方式的地址

statuc为NULL表示直接释放子进程PCB（PCB Process Control Block），不接收返回值

- WIFEXITED(status)：判断子进程是否正常结束
- WEXITSTATUS(status)：获取子进程返回值
- WIFSIGNALED(status)：判断子进程是否被信号结束
- WTERMSIG(status)：获取结束子进程的信号类型

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    int status = 0;
    pid_t pid = fork();
    if (pid < 0) {
        printf("fork process failed!\n");
        return 0;
    } else if (pid == 0) {
        printf("child pid is %d\n", getpid());
        sleep(1);
        exit(1);
    } else {
        printf("parent pid is %d\n", getpid());
        wait(&status);
        printf("child exit status is %d\n", WEXITSTATUS(status));
        return 0;
    }
}

```

### 6.2 waitpid函数

```c
#include <sys/types.h>
#include <sys/wait.h>
pid_t waitpid(pid_t pid, int *wstatus, int options);
```

pid可用于指定回收哪个子进程或任意子进程

函数参数：
	**pid**		pid是一个整数，具体的数值含义为：
	pid>0	回收PID等于参数pid的子进程
	pid==-1	回收任何一个子进程。此时同wait()
	pid==0	回收其组ID等于调用进程的组ID的任一子进程
	pid<-1	回收其组ID等于pid的绝对值的任一子进程
	**status**	同wait()
	**options**	
			0：同wait()，此时父进程会阻塞等待子进程退出
			WNOHANG：若指定的进程未结束，则立即返回0
	**返回值**：
		>0		已经结束运行的子进程号
		0		使用WNOHANG选项且子进程未退出
		-1		错误

```c
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
int main()
{
	pid_t pid;
	pid = fork();
	if(pid<0) {
		perror("cannot fork");
		return -1;
	} else if (pid==0) {
		printf("This is Child process\n");
		sleep(5);
		exit(0);
	} else {
		int ret;
		do {
			ret = waitpid(pid, NULL, WNOHANG);
			if (ret==0) {
				printf("The Child process is running, can't be exited\n");
				sleep(1);
			}
		} while (ret==0);
		if (ret == pid) {
			printf("Child process exited\n");
		} else {
			printf("Some error occured\n");
		}
	}
	return 0;
}
```

## 七、守护进程

### 7.1 守护进程概述

​		守护进程（daemon，也被译为“精灵进程”）是一类在后台工作的特殊进程，通常情况下，守护进程用于执行特定的系统任务。守护进程的生命周期较长，一些守护进程在系统引导时启动，一直运行至系统关闭；另一些守护进程在需要工作的时候启动，完成任务后就自动结束。在操作系统内，许多的系统服务都是通过守护进程实现的。
​		由于守护进程运行在后台，因此守护进程必须脱离前台（终端）运行。通常情况下，每一个从终端启动的进程都是依附于该终端的，当该终端关闭时，依附于该终端的进程也会自动结束。而守护进程却能够（或者说，必须）突破终端的限制，不受终端的影响在后台工作。从另一个角度说，若我们希望某些任务不因用户、终端或其他因素的变化而受影响，则必须把这个进程变成守护进程。

- Daemon进程是Linux三种进程类型之一
- 通常在系统启动时运行，系统关闭时结束
- Linux系统中很多服务程序是以守护进程形式运行

### 7.2 守护进程特点

- 始终在后台运行
- 独立于任何终端
- 周期性的执行某种任务或等待处理特定事件

### 7.3 会话、控制终端

- Linux以会话（session）、进程组的方式管理进程
- 每个进程属于一个进程组
- 会话：一个或多个进程组的集合。通常用户打开一个终端时，系统会创建一个会话。所有通过该终端运行的进程都属于这个会话，该终端也被称为该会话的控制终端，一个会话最多只能打开一个控制终端，也可以不打开
- 终端关闭时，所有相关进程会被结束

### 7.4 守护进程创建

- **创建子进程，父进程退出**

  - 子进程变成孤儿进程，被init进程收养
  - 子进程在后台运行

- **在子进程内创建新会话**

  原先的会话是打开一个终端时候创建的，进程创建的时候直接继承过来了

  - 子进程成为了新的会话组组长
  - 子进程脱离原先的终端

- **改变工作目录**

  - 守护进程一直在后台运行，其工作目录不能被卸载或删除，所以我们一般会指向一个不会删除的目录
  - 重新设定当前工作目录为"/"或者"/tmp"

- **重设文件权限掩码**

  我们创建的文件一般会收到系统权限的影响，在守护进程中我们有时候也需要创建一些文件，但是同时一般不希望受到影响，所以通常将umask设置为0

  - 文件权限掩码设置为0
  - 只影响当前进程

- **关闭文件描述符**

  - 关闭所有从父进程继承的打开文件
  - 已脱离终端，stdin/stdout/stderr无法再使用

```c
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<fcntl.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/stat.h>
#include<sys/wait.h>

int main()
{
	pid_t pid = fork();
	if (pid < 0) {
		perror("cannot fork");
		exit(1);
	} else if (pid > 0) {
		exit(0);//第一步：父进程退出
	}
	setsid(); //第二步：子进程创建新会话
	chdir("/tmp"); //第三步：改变工作目录为/tmp
	umask(0);//第四步：重设文件权限掩码
	for (int i = 0;i < getdtablesize(); i++) {
		close(i);
	}
	//此时守护进程创建完毕，下面开始守护进程的工作
	while (1) {
        // Todo
	}
	exit(0);
}
```

## 八、进程间通信

### 8.1 进程间通信简介

​		由于每个进程都有自己独立的运行环境，因此进程与进程间是相对封闭的。如何让两个封闭的进程之间实现数据通信是进程编程的重点与难点。
​		Linux内的进程通信机制基本来源于Unix系统。对Unix发展做出巨大贡献的两大主力——AT&T公司的贝尔实验室和加州大学伯克利分校——在进程通信领域研究的侧重点不同。
​		贝尔实验室对Unix系统早期的进程间通信手段进行了改进与扩充，形成了System V IPC（Inter-Process Communication，进程间通信）。互相通信的进程被限定在单个计算机内。而伯克利分校则跳出了System V IPC的限制，发展出了以套接字（socket）为基本点的进程间通信机制。

​		Linux系统将二者的优势全部继承下来

### 8.2 进程间通信方式

- 无名管道和有名管道
- 信号（signal）
- 消息队列（message queue）
- 共享内存（shared memory）
- 信号量（semaphore）
- 套接字（socket）

### 8.3 管道

​		管道是Linux中进程间通信的一种常用方式，它将一个程序的输出直接作为另一个程序的输入。Linux内的管道通信主要有无名管道（PIPE）与有名管道（FIFO）两种。

#### 8.3.1 无名管道（PIPE）

- 只能用于具有亲缘关系的进程间通信（父子进程、兄弟进程）
- 半双工通信模式，即无法同时读写管道。管道具有固定的读端与写端
- 管道可以看做特殊的文件，可以使用read()/write()函数对管道进行读写操作（但是不能使用lseek()进行定位操作）
- 管道不属于文件系统，并且只存放在内存中

**无名管道函数**

```c
#include<unistd.h>
int pipe(int pipefd[2]);
int pipe(int pipefd[]);
int pipe(int *pipefd);
```

**函数参数：**
		fd[]	包含两个文件描述符的数组，其中fd[0]固定用于读管道，fd[1]固定用于写管道
**函数返回值：**
		成功：0
		失败：-1

```c
#include <sys/types.h>
#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#define MAXLEN 100
int main()
{
	int n;
	int fd[2];
	pid_t pid;
	char msg[MAXLEN] = { 0 };
	if (pipe(fd) < 0) {
		perror("cannot create a pipe");
		return 0;
	}
	if ((pid = fork()) < 0) {
		perror("cannot fork");
		return 0;
	} else if (pid == 0) {
		close(fd[0]);
		sleep(1);
		strcpy(msg, "Helloworld\n");
		write(fd[1], msg, strlen(msg));
		close(fd[1]);
	} else {
		close(fd[1]);
		n = read(fd[0], msg, MAXLEN);
		printf("Parent read %d characters, Message is: %s", n, msg);
		close(fd[0]);
		waitpid(pid,NULL,0);
	}
	return 0;
}
```

### 8.3.2 有名管道（FIFO）

​		无名管道的使用范围比较狭隘，因为它只能实现有亲缘的进程之间的通信任务。如果想使用管道实现没有亲缘关系的进程间通信，我们可以采用有名管道的方式。
​		有名管道可以实现互不相干的两个进程之间的通信。有名管道在文件系统中可见，而且可以通过路径访问。进程通过文件IO的方式来读写管道内的数据。但是无法使用lseek()函数进行定位操作。

​		有名管道的原型来自于数据结构的队列，因此有名管道遵循“先进先出”的原则，这也是有名管道的名称(FIFO)的由来。

**有名管道函数**

```c
#include<sys/types.h>
#include<sys/stat.h>
int mkfifo(const char *pathname, mode_t mode)
```

**函数参数：**
		pathname	要创建的有名管道的路径名与文件名

​		mode		创建的有名管道的文件权限码，通常用八进制数字表示

**函数返回值：**
		成功：0
		失败：-1

```c
// create_fifo.c
#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
int main(int argc, const char *argv[])
{
	if(mkfifo("./fifo", 0664) < 0) {
		perror("cannot create fifo");
		return 0;
	}
	return 0;
}

// write_fifo.c
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<sys/stat.h>
#include<unistd.h>
#include<sys/types.h>
#define MAX 256
int main(int argc, const char *argv[])
{
	int fd;
	char buffer[MAX] = "Hello World";
	if ((fd = open("./fifo", O_WRONLY)) < 0) {
		perror("cannot open pipe");
		return 0;
	}
    write(fd, buffer, strlen(buffer) + 1);
	close(fd);
	return 0;
}

// read_fifo.c
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>
#include<fcntl.h>
#define MAX 256
int main(int argc, const char *argv[])
{
	int nread;
	char readbuffer[MAX] = {0};
	int fd;
	if ((fd = open("./fifo", O_RDONLY)) < 0)//打开管道。因为需要读管道，所以使用O_RDONLY
	{
		perror("cannot open pipe");
		return 0;
	}

    read(fd, readbuffer, MAX);
    printf("read string: %s\n", readbuffer);
	close(fd);
	return 0;
}
```

### 8.4 信号通信signal

这一节不讲，用的地方不多，自行学习

### 8.5 共享内存shared-memroy

​		共享内存就是允许两个不相关的进程访问同一个内存。共享内存是进程间最为高效的一种通信方式，进程间可以直接读写内存而无需使用其他的手段。
​		在Linux系统内，内核专门预留了一块内存区域用于进程间交换信息。这段内存区可以由任何需要访问的进程将其映射到自己的私有地址空间，进程可以直接读写该区域而无需数据拷贝，从而大大提高了效率。所有进程都有权访问共享内存地址，就好像使用malloc()函数分配的内存一样。但是，由于多个进程共享一段内存，因此一个进程改变共享内存内的数据可能会影响其他进程。由于共享内存本身并没有提供同步与互斥机制，因此需要依靠某种同步与互斥机制来保证数据的独立性，常见的手段是使用信号量来实现同步与互斥机制。

​		共享内存的使用分为两个步骤：第一步是创建共享内存，使用shmget()函数从内存中获取一块共享内存区域；第二步是映射共享内存，也就是使用shmat()把申请的共享内存区映射到具体的进程空间中。除此之外，还可以使用shmdt()函数撤销映射操作。

​		共享内存编程需要调用的常用几个函数：**ftok()、shmget()、shmat()、shmdt()和shmctl()**

#### 8.5.1 创建key键值对：ftok

参考文档：[ftok()函数深度解析](https://blog.csdn.net/u013485792/article/details/50764224)

**函数类型**

```c
#include <sys/types.h>
#include <sys/ipc.h>
key_t ftok(const char *pathname, int proj_id);
```

**函数功能**

​		生成IPC key

**函数参数**
		pathname			一个存在的文件路径，可以是目录，通常使用当前目录
		proj_id				 0-255一个数字，可以自己定义
**函数返回值**
		成功：共享内存段的标识符（非负整数）
		失败：-1

**Note：**使用ftok生成的key值切记要保证第一个参数pathname存在而且要保证不能被修改，删除重新创建都不行

#### 8.5.2 创建共享内存：shmget

​		在创建SystemV进程间通信（共享内存/消息队列/信号量）的函数中，都出现了key_t类型的key参数。该参数表示指定的键值，其他进程可以通过该键值访问该结构。不过有些情况下参数key的值是不能直接指定的（例如内核中已有该键值或键值非法），此时就需要使用ftok()函数生成一个符合要求的键值。
​		函数ftok()用于将路径名和当前进程标识符转换成符合SystemV的key值，该值是系统生成的，具有唯一性。因此在不能自定义指定参数key的时候，我们可以使用ftok()函数生成一个符合标准的key值。

**函数类型**

```c
#include <sys/ipc.h>
#include <sys/shm.h>
int shmget(key_t key, size_t size, int shmflg);
```

**函数参数**
		key			共享内存的键值，其他进程通过该值访问该共享内存，其中有个特殊值IPC_PRIVATE，表示创建当前进程的私有共享内存
		size			申请的共享内存段的大小
		shmflg		同open()函数的第三个参数，为共享内存设定权限，通常使用八进制表示。若共享内存不存在想创建一块全新的共享内存时，需要按位或IPC_CREAT
**函数返回值**
		成功：共享内存段的标识符（非负整数）
		失败：-1

**Note：**key值可以是IPC_PRIVATE，也可以是通过ftok生成的，也可以是自行定义的，例如：(key_t)0x1234

#### 8.5.3 共享内存映射：shmat

**函数类型**

```c
#include<sys/types.h>
#include<sys/shm.h>
void *shmat(int shmid, const void *shmaddr, int shmflg);
```

**函数参数**
		shmid	要映射的共享内存区标识符（即shmget()函数的返回值）
		shmaddr	将共享内存映射到的指定内存地址，如果为NULL则会自动分配到一块合适的内存地址，一般使用NULL
		shmflg	SHM_RDONLY表示共享内存为只读，0（默认值）表示共享内存可读可写
**函数返回值**
		成功：被映射的内存地址
		失败：-1

#### 8.5.4 将进程与共享内存分离：shmdt

**函数类型**

```c
#include<sys/types.h>
#include<sys/shm.h>
int shmdt(const void *shmaddr);
```

**函数参数**
		shmaddr	需要解除映射的共享内存地址
**函数返回值**
		成功：0
		失败：-1

#### 8.5.4 共享内存管理：shmctl

**函数类型**

```c
#include<sys/types.h>
#include<sys/shm.h>
int shmctl(int shmid, int cmd, struct shmid_ds *buf)
```

**函数参数**
		shmid	共享内存区标识符（即shmget()函数的返回值）
		cmd		需要对共享内存采取的操作。可取值有很多，常用的有：
			IPC_STAT	得到共享内存的状态，把共享内存的shmid_ds结构复制到buf中
			IPC_SET		如果进程权限允许，把buf所指的shmid_ds结构中的uid、gid、mode复制到共享内存的shmid_ds结构内
			IPC_RMID	删除共享内存
		buf		该参数是一个shmid_ds类型的结构体指针，使用时必须使用地址传递的方式。结构体成员很多，常用的有：
			struct shmid_ds
			{
				uid_t shm_perm.uid;		/* Effective UID of owner */
				uid_t shm_perm.gid;		/* Effective GID of owner */
				mode_t shm_perm.mode;	/* Permissions + SHM_DEST and SHM_LOCKED flags */
				……
			};
**函数返回值**
		成功：

​			IPC_INFO或SHM_INFO操作：内核内部记录的有关共享内存段的使用条目
​			SHM_STAT操作：shmid中指定的共享内存标识符
​			其他操作：0
​		失败：

​				-1，错误原因存于error中

​				EACCESS：参数cmd为IPC_STAT，确无权限读取该共享内存

​				EFAULT：参数buf指向无效的内存地址

​				EIDRM：标识符为msqid的共享内存已被删除

​				EINVAL：无效的参数cmd或shmid

​				EPERM：参数cmd为IPC_SET或IPC_RMID，却无足够的权限执行

**例一：创建共享私有的共享内存**

```c
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<string.h>
#include <sys/wait.h>
#include <unistd.h>

#define BUFFERSIZE 2048
int main()
{
	pid_t pid;
	int shmid; //共享内存标识符
	char *shm_addr;
	char flag[] = "WROTE";
	char buff[BUFFERSIZE];
	//创建共享内存
	if ((shmid = shmget(IPC_PRIVATE, BUFFERSIZE, 0664)) < 0) {
		perror("cannot shmget");
		return 0;
	}
	system("ipcs -m"); //显示当前共享内存情况
	pid = fork();//创建进程
	if (pid == -1) {
		perror("cannot fork");
		return 0;
	} else if (pid == 0) {
		//映射共享内存
		if ((shm_addr = shmat(shmid, NULL, 0)) == (void*)-1) {
			perror("Child Process: shmat");
			return 0;
		}
		system("ipcs -m");//显示当前共享内存情况
		while (strncmp(shm_addr, flag, strlen(flag))) {
			printf("Child Process is waiting for data……\n");
			sleep(3);
		}
		strcpy(buff, shm_addr + strlen(flag)); //获取数据，从WROTE后面开始读取
        system("ipcs -m");
		printf("Child Process read %s\n",buff);
		//解除共享内存映射
		if (shmdt(shm_addr) < 0) {
			perror("Child Process: shmdt");
			return 0;
		}
		system("ipcs -m");
		//删除共享内存
		if (shmctl(shmid, IPC_RMID,NULL) == -1) {
			perror("Child Process: shmctl(IPC_RMID)\n");
			return 0;
		}
	} else {
		//映射共享内存
		if((shm_addr = shmat(shmid, NULL, 0)) == (void*)-1) {
			perror("Parent Process: shmat");
			return 0;
		}
		sleep(1);
		printf("\nInput string:");
		strcpy(shm_addr,flag);//写入共享内存WROTE
		strncpy(shm_addr + strlen(flag), "Hello World", strlen("Hello World"));//写入共享内存用户输入数据
		//解除共享内存映射
		if (shmdt(shm_addr) < 0) {
			perror("Parent Process: shmdt");
			return 0;
		}
		system("ipcs -m");//显示当前共享内存情况
		waitpid(pid,NULL,0);//等待回收子进程
		printf("Finished\n");
	}
	return 0;
}
```

**例二：两个进程间的共享内存通信**

```c
// shmread.c
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<string.h>
#include <sys/wait.h>
#include <unistd.h>

#define TEXT_SZ 2048

struct SharedUseSt
{  
	int written; //作为一个标志，非0：表示可读，0表示可写
	char text[TEXT_SZ]; //记录写入和读取的文本
};

int main()
{  
	int running = 1;//程序是否继续运行的标志
	void *shm = NULL;//分配的共享内存的原始首地址
	struct SharedUseSt *shared;
    key_t key = ftok(".", 'a');
	int shmid;//共享内存标识符  
	//创建共享内存
	shmid = shmget(key, sizeof(struct SharedUseSt), 0666 | IPC_CREAT);  
	if (shmid == -1)  {  
		perror("shmget failed");  
		exit(0);  
	}  
	//将共享内存连接到当前进程的地址空间  
	shm = shmat(shmid, NULL, 0);  
	if (shm == (void*)-1) {  
		perror("shmat failed");  
		exit(0);  
	}
	printf("\nMemory attached at %p\n", shm);  
	//设置共享内存  
	shared = (struct SharedUseSt *)shm;  
	shared->written = 0;  
	while (running) {  
		// 没有进程向共享内存定数据有数据可读取  
		if (shared->written != 0)  {  
			printf("You wrote: %s\n", shared->text);
			//读取完数据，设置written使共享内存段可写  
			shared->written = 0;  
			//输入了end，退出程序
			if(strncmp(shared->text, "end", 3) == 0)  
			running = 0;  
		} else {//有其他进程在写数据，不能读取数据  
			sleep(1);
        } 
	}  
	//把共享内存从当前进程中分离  
	if (shmdt(shm) == -1)  {  
		perror("shmdt failed");  
		exit(0);  
	}  
	//删除共享内存
	if(shmctl(shmid, IPC_RMID, 0) == -1) {
		perror("shmctl(IPC_RMID) failed");
		exit(0);  
	}
	return 0;
}

// shmwrite.c
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<string.h>
#include <sys/wait.h>
#include <unistd.h>

#define TEXT_SZ 2048

struct SharedUseSt
{  
	int written; //作为一个标志，非0：表示可读，0表示可写
	char text[TEXT_SZ]; //记录写入和读取的文本
};

int main()
{  
	int running = 1;
	void *shm = NULL;
	struct SharedUseSt *shared = NULL;
	char buffer[TEXT_SZ + 1];//用于保存输入的文本
	int shmid;
    key_t key = ftok(".", 'a');
	//创建共享内存
	shmid = shmget(key, sizeof(struct SharedUseSt), 0666 | IPC_CREAT);
	if (shmid == -1) {
		perror("shmget failed");
		exit(0);
	}
	//将共享内存连接到当前进程的地址空间  
	shm = shmat(shmid, NULL, 0);
	if (shm == (void*)-1) {  
		perror("shmat failed");
		exit(0);
	}
	printf("Memory attached at %p\n", shm);
	//设置共享内存
	shared = (struct SharedUseSt*)shm;
	while (running) {
		//数据还没有被读取，则等待数据被读取,不能向共享内存中写入文本  
		while (shared->written == 1) {
			sleep(1);
			printf("Waiting...\n");
		}
		//向共享内存中写入数据
		printf("Enter some text: ");
		fgets(buffer,TEXT_SZ,stdin);
		strcpy(shared->text, buffer);
		//写完数据，设置written使共享内存段可读
		shared->written = 1;
		//输入了end，退出循环（程序）
		if (strncmp(buffer, "end", 3) == 0) {
			running = 0;
        }
	}
	//把共享内存从当前进程中分离
	if(shmdt(shm) == -1) {
		perror("shmdt failed");
		exit(0);
	}
	sleep(2);
	return 0;
}

```

