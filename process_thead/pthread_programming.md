# 线程编程

## 一、线程简介

### 1.1 线程的基本概念

​		进程是系统中资源执行和资源分配的最小单位。每个进程都有自己独立的数据区、代码区、堆栈区等，这就造成了当进程切换时，操作系统需要额外的操作来清空旧区域、分配新区域，进程在进行切换时的系统开销比较大。为了提高效率，绝大多数操作系统都提供了一种“轻量化进程”的概念，这就是线程的概念。

​		线程（thread），也被称为轻量级进程，是进程内一个相对独立的、可被进程调度的执行单元。若在单个程序中同时运行多个线程完成工作，则称为多线程编程。

### 1.2 线程与进程的区别

​		一个进程可以拥有多个线程，每个线程共享该进程内的系统资源。由于线程共享进程的内存空间，因此任何线程对内存内数据的操作都可能对其他线程产生影响，因此多线程的同步与互斥机制是十分重要的。

​		线程本身只占用少量的系统资源，其内存空间也只拥有堆栈区与线程控制块（Thread Control Block，简称TCB），因此对线程的调度需要的系统开销会小得多，能够更高效地提高任务的并发度。

**进程与线程的区别：**

- 地址空间与系统资源：进程间的地址空间与系统资源互相独立，互不干扰；同一进程内各线程共享地址空间与系统资源。一个进程内的线程对其他进程是不可见（私有）的。
- 通信手段：由于进程间互相独立，因此进程间通信必须借助某些手段。进程间通信手段主要有管道、信号、共享内存、SystemV等；而线程共享进程的资源与空间，因此同一个进程的线程间可以直接读写进程的数据段（例如全局变量等）进行通信，不过需要使用同步与互斥机制保证数据一致性。
- 调度与切换：进程占用系统资源较多，因此切换进程时开销较大；而线程占用系统资源较小，因此切换进程时开销较小。

### 1.3 线程的资源

**一个进程中的多个线程共享资源：**

- 执行的命令
- 静态数据（例如全局变量等）
- 打开文件的文件描述符
- 信号处理函数
- 当前工作目录

**每个线程私有的资源有：**

- 线程标识符（简称线程号，TID）
- 程序计数器（PC）与相关寄存器
- 堆栈区（局部变量、函数返回地址等）
- 错误号errno
- 信号掩码与优先级
- 执行状态与属性

**线程核心两部分工作：**

- 线程的创建、控制与删除
- 线程的同步与互斥。

### 1.4 NPTL简介

​		本地POSIX线程库（New POSIX Thread Library，简称NPTL）是早期Linux系统内Threads模型的改进，它可以让Linux内核高效运行使用POSIX风格编写的线程程序。有测试证明，使用NPTL启动10万个线程大概只需2秒时间，而未使用NPTL则需要15分钟。
​		NPTL最先发布在RedHat9.0版本中（2003年），老式POSIX线程库的效率太低，因此从这个版本开始，NPTL开始取代老式Linux线程库。

​		NPTL线程库需要提前先安装才能使用：

```shell
sudo apt-get install glibc-doc
sudo apt-get install manpages-posix-dev
```

## 二、线程编程

### 2.1 线程创建

```c
#include<pthread.h>
int pthread_create(pthread_t *thread, pthread_attr_t *attr, void *(*routine)(void *), void *arg)
```

**函数参数：**
		thread	创建线程的标识符
		attr	线程属性设置，如设置成NULL则为缺省（default）属性
		routine	线程执行的函数
		arg		传递给routine的参数
**函数返回值：**
		成功：0
		失败：返回错误码

### 2.2 线程退出

```c
#include<pthread.h>
void pthread_exit(void *retval)
```

**函数参数：**
		retval	线程结束时的返回值，可以通过pthread_join()接收

**Note：**退出线程需要使用pthread_exit()函数，这个函数属于线程的主动行为，不能使用exit()函数试图退出线程，因为exit()函数的作用是使当前进程终止，如果某个线程调用了exit()函数，则会使得进程退出，该进程的所有线程都会直接终止。

### 2.3 等待线程

进程与进程之间，父进程使用wait()函数来等待回收子进程，线程内也有类似的机制，使用pthread_join()函数将一直等待到指定的线程结束为止。

```c
#include<pthread.h>
int pthread_join(pthread_t thread, void **thread_result)
```

**函数参数：**
		thread			等待线程的标识符
		thread_result	用户定义的指针，当不为NULL时用来接收等待线程结束时的返回值，即pthread_exit()函数内的retval值
**函数返回值：**
		成功：0
		失败：返回错误码

### 2.4 取消线程

我们可以使用pthread_exit()函数使得线程主动结束。实际应用中，我们经常需要让一个线程去结束另一个线程，此时可以使用pthread_cancel()函数来实现这样的功能。当然，被取消的线程内部需要事先设置取消状态，可以使用pthread_setcancelstate()函数或pthread_setcanceltype()函数来设置线程被取消的状态。

```c
#include<pthread.h>
int pthread_cancel(pthread_t thread)
```

**函数参数：**
		thread			等待线程的标识符
**函数返回值：**
		成功：0
		失败：返回错误码

### 2.5 线程取消状态

```c
#include<pthread.h>
int pthread_setcancelstate(int state, int *oldstate);
```

**函数参数：**		

​		state			设置线程是否可取消：PTHREAD_CANCEL_ENABLE/PTHREAD_CANCEL_DISABLE
​		oldstate		使用NULL
**函数返回值：**
​		成功：0
​		失败：返回错误码

### 2.6 线程取消方式

```c
#include<pthread.h>
int pthread_setcanceltype(int type, int *oldtype);
```

**函数参数：**		

​		type			设置线程取消方式：PTHREAD_CANCEL_ASYNCHRONOUS异步取消；PTHREAD_CANCEL_DEFERRED延迟取消
​		oldstate		使用NULL
**函数返回值：**
​		成功：0
​		失败：返回错误码

### 2.7 获取当前线程标识符

```c
#include<pthread.h>
pthread_t pthread_self(void)
```

**函数返回值：**
		当前线程的线程标识符

**例一：**创建单进程

```c
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
pthread_t tid;
void *thrd_function(void *arg)
{
	printf("New Process: PID:%d, TID:%u.\n", getpid(), tid);
	pthread_exit(NULL);
}
int main()
{
	if(pthread_create(&tid,NULL,thrd_function,NULL)!=0) {
		printf("Create thread error!\n");
		exit(0);
	}
	printf("Main Process: PID:%d, TID in pthread_create function %u.\n", getpid(), tid);
	sleep(1);
	return 0;
}
```

**例二：**创建多进程随机运行

```c
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
pthread_t tid1, tid2, tid3;
void *thrd_function1(void *arg)
{
	printf("This is 1st thread:\n");
	printf("1st TID:%u.\n",tid1);
	printf("1st thread will exit\n");
	pthread_exit(NULL);
}
void *thrd_function2(void *arg)
{
	printf("This is 2nd thread:\n");
	printf("2nd thread will print string:%s\n",(char*)arg);
	printf("2nd thread will exit\n");
	pthread_exit(NULL);
}
void *thrd_function3(void *arg)
{
	printf("This is 3rd thread:\n");
	printf("3rd thread will calculate:1+2+3+……+100\n");
	int i, sum;
	for(i = 0,sum = 0;i <= 100;i++) {
		sum += i;
	}
	printf("sum is %d\n",sum);
	printf("3rd thread will exit\n");
	pthread_exit(NULL);
}
int main()
{
	if(pthread_create(&tid1, NULL, thrd_function1, NULL) != 0) {
		printf("Create thread1 error!\n");
		exit(0);
	}
	if(pthread_create(&tid2, NULL, thrd_function2, "helloworld") != 0) {
		printf("Create thread2 error!\n");
		exit(0);
	}
	if(pthread_create(&tid3, NULL, thrd_function3, NULL) != 0) {
		printf("Create thread3 error!\n");
		exit(0);
	}
	sleep(1);
	return 0;
}
```

**例三：**创建多进程控制执行顺序

```shell
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
pthread_t tid1, tid2, tid3;
void *tret;
void *thrd_function1(void *arg)
{
	if(pthread_join(tid2, &tret) != 0) {
		printf("Join thread 2 error\n");
		exit(0);
	}
	printf("Thread 2 exit code:%d\n", (int)tret);
	printf("This is 1st thread:\n");
	printf("1st TID:%u.\n", tid1);
	printf("1st thread will exit\n");
	pthread_exit((void *)1);//退出线程
}
void *thrd_function2(void *arg)
{
	printf("This is 2nd thread:\n");
	printf("2nd thread will print string:%s\n", (char*)arg);
	printf("2nd thread will exit\n");
	pthread_exit((void*)2);
}
void *thrd_function3(void *arg)
{
	if(pthread_join(tid1,&tret)!=0) {
		printf("Join thread 1 error\n");
		exit(0);
	}
	printf("Thread 1 exit code:%d\n",(int)tret);
	printf("This is 3rd thread:\n");
	printf("3rd thread will calculate:1+2+3+……+100\n");
	int i,sum;
	for(i=0,sum=0;i<=100;i++) {
		sum+=i;
	}
	printf("sum is %d\n",sum);
	printf("3rd thread will exit\n");
	pthread_exit((void*)3);
}
int main()
{
	if(pthread_create(&tid1,NULL,thrd_function1,NULL)!=0) {
		printf("Create thread1 error!\n");
		exit(0);
	}
	if(pthread_create(&tid2,NULL,thrd_function2,"helloworld")!=0) {
		printf("Create thread2 error!\n");
		exit(0);
	}
	if(pthread_create(&tid3,NULL,thrd_function3,NULL)!=0) {
		printf("Create thread3 error!\n");
		exit(0);
	}
	if(pthread_join(tid3,&tret)!=0) {
		printf("Join thread 3 error\n");
		exit(0);
	}
	printf("Thread 3 exit code:%d\n",(int)tret);
	printf("This is Main Process %d\n",getpid());
	sleep(1);
	return 0;
}
```

## 三、线程同步和互斥

​		与进程需要专用的通信手段不同，线程与线程之间由于共享进程的资源与地址空间，因此线程间可直接通信（类似函数与函数间使用全局变量传递数据）。但是由于多个线程共同使用资源与地址空间，因此必须考虑线程间资源访问的同步与互斥。
​		NPTL线程库有两种常用的线程间同步与互斥的机制：互斥锁与信号量。互斥锁比较适合同时可用的资源是唯一的情况，而信号量比较适合同时可用的资源为多个的情况。

### 3.1 线程控制——互斥锁mutex

​		互斥锁通过简单的加锁方法来保证对共享资源的原子操作。互斥锁只有两种状态：上锁与解锁。在同一时刻只能有一个线程使用互斥锁，拥有互斥锁的线程可以对线程的共享资源进行访问直至解锁；其他未获得互斥锁的线程在此期间必须等待直至有线程解锁为止。互斥锁函数都是含有"mutex"字样的函数。

​		原子操作（atomic operation）指不会被线程调度机制打断的操作；这种操作一旦开始，就一直运行到结束，中间不会有任何线程切换动作。原子操作可以是一个步骤，也可以是多个操作步骤，但是其顺序不可以被打乱，也不可以被切割而只执行其中的一部分。将整个操作视作一个整体是原子性的核心特征。

### 3.2 初始化互斥锁

```c
#include<pthread.h>
int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr)
```

**函数参数：**
		mutex		互斥锁
		mutexattr	互斥锁的属性，如设置成NULL则为缺省（default）属性
**函数返回值：**
		成功：0
		失败：返回错误码

### 3.3 阻塞上锁锁

```c
#include<pthread.h>
int pthread_mutex_lock(pthread_mutex_t *mutex)
```

不成功直接等待

### 3.4 非阻塞上锁

```c
#include<pthread.h>
int pthread_mutex_trylock(pthread_mutex_t *mutex)
```

不成功直接返回失败

### 3.5 解锁

```c
#include<pthread.h>
int pthread_mutex_unlock(pthread_mutex_t *mutex)
```

### 3.6 删除互斥锁

```c
#include<pthread.h>
int pthread_mutex_destroy(pthread_mutex_t *mutex)
```

例一：

```c
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include<math.h>
#define THREAD_NUM	3	/*线程数*/
#define REPEAT_NUM	5	/*每个线程执行的循环次数*/
#define DELAY_TIME	6	/*每次循环的最大间隔*/
pthread_mutex_t mutex;	//互斥锁
int b = 0;
void *thrd_function(void *arg)
{
	int thrd_num = (int)arg;
	int delay_time = 0;
	int count = 0;
	if(pthread_mutex_lock(&mutex)) {
		printf("Thread %d lock failed\n",thrd_num);
		pthread_exit(NULL);//如果上锁失败则直接结束线程
	}
	printf("Thread %d is running!\n",thrd_num);
	for(count=0;count<REPEAT_NUM;count++) {
		delay_time = rand()%DELAY_TIME+1;//随机生成1~6，代表当次的等待时间
		sleep(delay_time); //延时
        b++;
		printf("\tThread %d: job %d delay=%d\n",thrd_num,count,delay_time);
	}
	printf("Thread %d finished\n",thrd_num);
	pthread_mutex_unlock(&mutex); //解锁，让其余线程运行
	pthread_exit(NULL);
}
void *thrd_function2(void *arg)
{
	int thrd_num = (int)arg;
	int delay_time = 0;
	int count = 0;
	if(pthread_mutex_lock(&mutex)) {
		printf("Thread %d lock failed\n",thrd_num);
		pthread_exit(NULL);//如果上锁失败则直接结束线程
	}
	printf("Thread %d is running!\n",thrd_num);
	for(count=0;count<REPEAT_NUM;count++) {
		delay_time = rand()%DELAY_TIME+1;//随机生成1~6，代表当次的等待时间
		sleep(delay_time); //延时
        b++;
		printf("\tThread %d: job %d delay=%d\n",thrd_num,count,delay_time);
	}
	printf("Thread %d finished\n",thrd_num);
	pthread_mutex_unlock(&mutex); //解锁，让其余线程运行
	pthread_exit(NULL);
}
int main()
{
	pthread_t thread[THREAD_NUM];
	int no = 0;
	void *thrd_ret;
	srand(time(NULL));
	pthread_mutex_init(&mutex,NULL);//互斥锁初始化，否则无法使用
	for(no=0;no<THREAD_NUM;no++) {
		//创建多线程
		if(pthread_create(&thread[no],NULL,thrd_function,(void*)no)!=0)
		{
			printf("Create thread %d error!\n",no);
			exit(0);
		}
	}
	printf("Create all threads success, Waiting threads to finish……\n");
	for(no=0;no<THREAD_NUM;no++) {
		if(pthread_join(thread[no],&thrd_ret)!=0)//等待线程结束
		{
			printf("Join thread %d error\n",no);
			exit(0);
		} else {
			printf("Thread %d has been joined by MainProcess\n",no);//表示主函数进程在等待该线程结束
		}
	}
	pthread_mutex_destroy(&mutex);//删除互斥锁
	return 0;
}
```

## 四、信号量semaphore

​		使用互斥锁可以实现两个线程之间的互斥操作，但是无法真正实现线程间的同步操作。此时我们可以使用信号量控制线程。信号量的函数都是含有"sem"字样的函数。
​		信号量是最早出现的用来解决进程同步与互斥问题的机制，包括一个称为信号量的变量及对它进行的两个原语操作（PV操作）。

​		1962年，Dijkstra教授来到荷兰南部的艾恩德霍芬技术大学(Eindhoven Technical University）任数学教授。在这里，他参加了X86计算机的开发。针对操作系统需要同步的问题，他提出了一种称为“PV操作”的同步机制。
Dijkstra教授巧妙地利用火车运行控制系统中的“信号灯”(semaphore，或叫“信号量”)概念加以解决。例如有进程P1与P2，为了防止这两个进程并发时产生错误，狄克斯特拉设计了一种同步机制叫“PV操作”，P操作和V操作是执行时不被打断的两个操作系统原语。由于Dijkstra教授使用荷兰语，在荷兰语中，“通过”叫“passeren”，“释放”叫“vrijgeven”，PV操作因此得名，这是极少数的在计算机科学中不使用英语表达的例子之一
​	P操作（通过）：对信号量减1，若结果大于等于0，则进程继续，否则执行P操作的进程被阻塞等待释放
​	V操作（释放）：对信号量加1，若结果小于等于0，则唤醒队列中一个因为P操作而阻塞的进程，否则不必唤醒进程

一般情况下，对于同一信号量，先执行P操作，然后执行V操作。PV操作成对出现。

- `int sem_wait(sem_t *sem);`等待信号量，如果信号量的值大于0,将信号量的值减1,立即返回。如果信号量的值为0,则线程阻塞。相当于P操作。成功返回0,失败返回-1。
- `int sem_post(sem_t *sem);` 释放信号量，让信号量的值加1。相当于V操作。

**例一：两个进程同步操作**

主函数进程从键盘读入用户输入的字符串，若字符串是"quit"则程序结束，否则线程计算输入字符串的长度。注意该示例中主函数进程与创建的线程之间的协同运行。注意信号量处理函数的用法。

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>   
#include <semaphore.h>
char buf[60];
sem_t sem;
void *function(void *arg)     
{
	printf("This is thread2\n");
	while(1)
	{
		if(sem_wait(&sem)==0)//P操作
		{
			printf("Thread2 is waiting……\n");
		}
		printf("You enter %d characters\n", strlen(buf) - 1);
	}
}
int main()
{
	pthread_t a_thread;
    void *thread_result;
	if(sem_init(&sem, 0, 0) < 0)//注意第二个参数只能填0
	{
		perror("fail to sem_init");
		exit(-1);
	}
	printf("This is MainProcess\n");
	if(pthread_create(&a_thread, NULL, function, NULL) < 0)
	{         
		perror("fail to pthread_create");     
		exit(-1);
	}
    sleep(1);
	printf("input 'quit' to exit\n");
	do {
		fgets(buf, 60, stdin);
		sem_post(&sem);	//V操作
	} while(strncmp(buf, "quit", 4) != 0);
	sem_destroy(&sem);
	sleep(1);
	return 0; 
}
```

**例二：公交车司机——公交车售票员**

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>   
#include <semaphore.h>
pthread_t tid1,tid2;
sem_t sem1,sem2;
void *bus_driver(void *arg)//司机线程  
{
	printf("司机线程开始运行……\n");
	while(1)
	{
		if(sem_wait(&sem1)==0)
		{
			printf("司机发现关闭车门\n");
		}
		printf("司机启动车辆\n");
		sleep(1);
		printf("司机驾驶车辆\n");
		sleep(10);//开车时间稍长方便观察程序运行效果
		printf("车辆到站，司机停车\n");
		sem_post(&sem2);
	}
}
void *bus_conductor(void *arg)//售票员线程
{
	printf("售票员线程开始运行……\n");
	while(1)
	{
		printf("售票员关闭车门\n");
		sleep(1);
		sem_post(&sem1);
		printf("售票员开始卖票\n");
		sleep(5);//卖票时间稍长方便观察程序运行效果
		printf("售票员卖票完毕\n");
		if(sem_wait(&sem2)==0)
		{
			printf("售票员发现车辆到站\n");
		}
		printf("车辆到站，售票员开车门\n");
		printf("售票员让乘客上下车\n");
		sleep(1);
	}
}

int main(int argc, const char *argv[])
{
	if(sem_init(&sem1, 0, 0) < 0)
	{
		perror("fail to sem_init");
		exit(-1);
	}
	if(sem_init(&sem2, 0, 0) < 0)
	{
		perror("fail to sem_init");
		exit(-1);
	}
	if(pthread_create(&tid1,NULL,bus_driver,NULL)!=0)
	{
		printf("Create thread1 error!\n");
		exit(0);
	}
	if(pthread_create(&tid2,NULL,bus_conductor,NULL)!=0)
	{
		printf("Create thread2 error!\n");
		exit(0);
	}
	if(pthread_join(tid1,NULL)!=0)
	{
		printf("Join thread 1 error\n");
		exit(0);
	}
	if(pthread_join(tid2,NULL)!=0)
	{
		printf("Join thread 2 error\n");
		exit(0);
	}
	sem_destroy(&sem1);
	sem_destroy(&sem2);
	sleep(1);
	return 0; 
}
```

**练习题：**

有一家四口：父亲、母亲、儿子、女儿。一天儿子想吃橘子，但是女儿想吃苹果。父亲负责供应苹果，母亲负责供应橘子。但是盛水果的盘子在同一时刻只能放下一个水果。使用线程的互斥与同步模拟一家四口放水果/吃水果的模型
	父亲--1个苹果-->盘子--1个苹果-->女儿
					或者
	母亲--1个橘子-->盘子--1个橘子-->儿子

代码填写

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>   
#include <semaphore.h>
pthread_t tid_dad,tid_mom,tid_son,tid_dau;
sem_t plate,apple,orange;
void *dad(void *arg)
{
	printf("父亲开始放入苹果……\n");
	while(1)
	{
		sleep(1);
		if(sem_wait(__________)==0)
		{
			printf("父亲放入一个苹果\n");
		}
		sleep(1);
		sem_post(&apple);
	}
}
void *mom(void *arg)
{
	printf("母亲开始放入橘子……\n");
	while(1)
	{
		sleep(1);
		if(sem_wait(__________)==0)
		{
			printf("母亲放入一个橘子\n");
		}
		sleep(1);
		sem_post(__________);
	}
}
void *son(void *arg)
{
	printf("儿子想吃橘子……\n");
	while(1)
	{
		if(sem_wait(__________)==0)
		{
			printf("儿子发现盘子内有橘子\n");
		}
		printf("儿子拿走橘子\n");
		sleep(1);
		sem_post(__________);
		printf("儿子吃掉橘子\n");
		sleep(1);
	}
}
void *dau(void *arg)
{
	printf("女儿想吃苹果……\n");
	while(1)
	{
		if(sem_wait(__________)==0)
		{
			printf("女儿发现盘子内有苹果\n");
		}
		printf("女儿拿走苹果\n");
		sleep(1);
		sem_post(__________);
		printf("女儿吃掉苹果\n");
		sleep(1);
	}
}
int main()
{
	if(sem_init(&plate,0,1)<0)
	{
		perror("fail to sem_init plate");
		exit(-1);
	}
	if(sem_init(&apple,0,0)<0)
	{
		perror("fail to sem_init apple");
		exit(-1);
	}
	if(sem_init(&orange,0,0)<0)
	{
		perror("fail to sem_init orange");
		exit(-1);
	}
	if(pthread_create(&tid_dad,NULL,dad,NULL)!=0)
	{
		printf("Create thread-dad error!\n");
		exit(0);
	}
	if(pthread_create(&tid_mom,NULL,mom,NULL)!=0)
	{
		printf("Create thread-mom error!\n");
		exit(0);
	}
	if(pthread_create(&tid_son,NULL,son,NULL)!=0)
	{
		printf("Create thread-son error!\n");
		exit(0);
	}
	if(pthread_create(&tid_dau,NULL,dau,NULL)!=0)
	{
		printf("Create thread-dau error!\n");
		exit(0);
	}
	if(pthread_join(tid_dad,NULL)!=0)
	{
		printf("Join thread-dad error\n");
		exit(0);
	}
	if(pthread_join(tid_mom,NULL)!=0)
	{
		printf("Join thread-mom error\n");
		exit(0);
	}
	if(pthread_join(tid_son,NULL)!=0)
	{
		printf("Join thread-son error\n");
		exit(0);
	}
	if(pthread_join(tid_dau,NULL)!=0)
	{
		printf("Join thread-dau error\n");
		exit(0);
	}
	sem_destroy(&plate);
	sem_destroy(&apple);
	sem_destroy(&orange);
	sleep(1);
	return 0;
}
```

